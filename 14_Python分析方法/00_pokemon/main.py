
# 引入必要的包
import csv
import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


#%%

# 解决matplotlib显示中文问题

# 实训平台解决方法
from matplotlib.font_manager import *
myfont = FontProperties(fname = './simhei.ttf')
plt.rcParams['font.sans-serif'] = ['SimHei']
# 运行以上代码
# 在以下的绘图过程中，如若需要设置 xlabel、ylabel，请在函数中添加参数 fontproperties = myfont 即可
# 例如：
#plt.xlabel('时间', fontproperties = myfont)
#plt.ylabel('数量', fontproperties = myfont)



# TODO
# 读取数据：data文件夹中的pokemon.csv
# 打印前五行
pokemonData = pd.read_csv('./data/pokemon.csv')
pokemonData.head(5)


#%% md

### Type_1 的数量统计图

#%%

# TODO
# 绘制 Type_1 的数量统计图
# 使用sns.countplot()
# 将title设置为主要类别的数量统计，xlabel设置为主要类别，ylabel设置为数量
# xlabel、y'la'b
ax_type1 = sns.countplot(x='Type_1', data=pokemonData)
plt.xlabel('主要类别', fontproperties = myfont)
plt.ylabel('数量', fontproperties = myfont)

plt.show()
# 在本机运行时，解决中文问题请参考以下代码
# 仅适用于Windows
# plt.rcParams['font.sans-serif'] = ['SimHei']  # 指定默认字体
# plt.rcParams['axes.unicode_minus'] = False  # 解决保存图像是负号'-'显示为方块的问题

# MacOS请参考 http://wenda.chinahadoop.cn/question/5304 修改字体配置