# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


class ChoutiFilePipeline(object):
    def open_spider(self,spider):
        print('我开了')
        self.file=open('chouti.txt','w',encoding='utf-8')
    def process_item(self, item, spider):
        print('111')
        # with open('chout.txt','w',encoding='utf-8') as f:
        #     f.write(item['title']+'\n')
        #     f.write(item['url']+'\n')
        #     f.write(item['photo_url']+'\n')
        self.file.write(item['title']+'\n')
        self.file.write(item['url']+'\n')
        self.file.write(item['photo_url']+'\n')
        return item
    def close_spider(self,spider):
        print('我关了')
        self.file.close()

import pymysql
class ChoutiMysqlPipeline(object):
    def open_spider(self,spider):
        self.conn=pymysql.connect( host='127.0.0.1', user='root', password="root",
                 database='testing', port=3306)
    def close_spider(self,spider):
        self.conn.close()
    def process_item(self, item, spider):
        cursor=self.conn.cursor()
        sql='insert into article (title,url,photo_url)values(%s,%s,%s) '
        cursor.execute(sql,[item['title'],item['url'],item['photo_url']])
        self.conn.commit()
        return item