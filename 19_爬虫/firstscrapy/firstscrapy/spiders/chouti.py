# -*- coding: utf-8 -*-
import scrapy
from scrapy.http.request import Request
from bs4 import BeautifulSoup

from firstscrapy.items import ChoutiItem
class ChoutiSpider(scrapy.Spider):
    name = 'chouti'
    allowed_domains = ['dig.chouti.com']
    start_urls = ['http://dig.chouti.com/']

    ####使用第三方解析
    def parse(self, response):
        print(response.text)
        # 解析数据(第一种方案，自己解析，bs4，lxml)
        soup=BeautifulSoup(response.text,'lxml')
        divs=soup.find_all(class_='link-title')
        for div in divs:
            print(div.text)

    #想继续爬取其他网址
    # def parse(self, response):
    #     # 以后解析都在这
    #     print(response.status)
    #     # 假设解析出一个网址（继续爬取）
    #     return Request('https://www.baidu.com/',dont_filter=True)


    ### 使用自带解析
    '''
    xpath：
        -response.xpath('xpath语法').extract()
        -response.xpath('xpath语法').extract_first()
        -response.xpath('//a[contains(@class,"link-title")]/text()').extract()  # 取文本
        -response.xpath('//a[contains(@class,"link-title")]/@href').extract()  #取属性
    -css
        -response.css('.link-title::text').extract()  # 取文本
        -response.css('.link-title::attr(href)').extract_first()  # 取属性
    '''
    # def parse(self, response):
    #     # 只有css和xpath
    #     # title_list=response.css('.link-title')
    #     # title_list=response.xpath('//a[contains(@class,"link-title")]/text()').extract()
    #     # title_list=response.xpath('//a[contains(@class,"link-title")]').extract()
    #     # print(len(title_list))
    #     # print(title_list)
    #
    #     # 解析出所有的标题和图片地址
    #     div_list=response.xpath('//div[contains(@class,"link-item")]')
    #     for div in div_list:
    #         #extract() 取出列表，即便有一个也是列表
    #         #extract_first() 取出第一个值
    #         # title=div.css('.link-title::text').extract()  #
    #         # title=div.css('.link-title::text').extract_first()
    #         # url=div.css('.link-title::attr(href)').extract()[0]
    #         # url=div.css('.link-title::attr(href)').extract_first()
    #         url=div.xpath('//a[contains(@class,"link-title")]/@href').extract_first()
    #         # print(title)
    #         print(url)


    # 持久化方案1
    # def parse(self, response):
    #     ll=[]
    #     div_list=response.xpath('//div[contains(@class,"link-item")]')
    #     for div in div_list:
    #         title=div.css('.link-title::text').extract_first()
    #         url=div.css('.link-title::attr(href)').extract_first()
    #         photo_url=div.css('.image-scale::attr(src)').extract_first()
    #         # 持久化：方案一（用的少）parser必须返回列表套字典的形式
    #         ll.append({'title':title,'url':url,'photo_url':photo_url})
    #     return ll

    # 持久化方案二
    # def parse(self, response):
    #
    #     div_list=response.xpath('//div[contains(@class,"link-item")]')
    #     for div in div_list:
    #         item = ChoutiItem()
    #         title=div.css('.link-title::text').extract_first()
    #         url=div.css('.link-title::attr(href)').extract_first()
    #         photo_url=div.css('.image-scale::attr(src)').extract_first()
    #         if not photo_url:
    #             photo_url=''
    #         # item.title=title
    #         # item.url=url
    #         # item.photo_url=photo_url
    #         item['title']=title
    #         item['url']=url
    #         item['photo_url']=photo_url
    #         yield item
