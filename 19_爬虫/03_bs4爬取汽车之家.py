
import requests
# pip3 install beautifulsoup4  解析html和xml，修改html和xml
from bs4 import BeautifulSoup

res=requests.get('https://www.autohome.com.cn/news/1/#liststart')
# print(res.text)
# 第二个参数，使用什么解析器
#html.parser内置，不需要安装第三方模块
# soup=BeautifulSoup(res.text,'html.parser')
# pip3 install lxml
soup=BeautifulSoup(res.text,'lxml')

# 查找class为article-wrapper的div
# div=soup.find(class_='article-wrapper')
# div=soup.find(id='auto-channel-lazyload-article')
# print(div)
ul=soup.find(class_='article')
# print(ul)
# 继续找ul下的s所有li
li_list=ul.find_all(name='li')
# print(len(li_list))
for li in li_list:
    # 找每个li下的东西
    title=li.find(name='h3')
    if title:
        title=title.text
        # url=li.find('a')['href']
        url='https:'+li.find('a').attrs.get('href')
        desc=li.find('p').text
        img='https:'+li.find(name='img').get('src')
        print('''
        新闻标题：%s
        新闻地址：%s
        新闻摘要：%s
        新闻图片：%s
        
        '''%(title,url,desc,img))
