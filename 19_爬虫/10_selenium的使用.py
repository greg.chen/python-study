
# pip3 install selenium


# 浏览器驱动:http://npm.taobao.org/mirrors/chromedriver/
# 驱动要跟浏览器版本对应  84.0.4147.105：驱动用84.0.4147.30/
# 下载完解压就是个exe（不同平台的可执行文件）
# from selenium import webdriver
# import time
# # bro=webdriver.Chrome()  # 得到一个谷歌浏览器对象，
# # 指定使用跟那个驱动
# bro=webdriver.Chrome(executable_path='./chromedriver.exe') # 得到一个谷歌浏览器对象，
#
# time.sleep(2)
# bro.get('https://www.baidu.com/')  # 在地址栏里输入了百度
# time.sleep(2)
# print(bro.page_source)
# time.sleep(2)
# bro.close()



# 模拟登陆百度
# from selenium import webdriver
# import time
# bro=webdriver.Chrome(executable_path='./chromedriver.exe')
#
# bro.get('https://www.baidu.com/')
# time.sleep(0.01)
# input_k=bro.find_element_by_id('kw')
# input_k.send_keys('美女')  # 在框里写入美女
# time.sleep(2)
# sou=bro.find_element_by_id('su')  # 找到搜索按钮
# sou.click() # 点击搜索按钮
# time.sleep(4)
# bro.close()


# from selenium import webdriver
# import time
# bro=webdriver.Chrome(executable_path='./chromedriver.exe')
# bro.implicitly_wait(5)  # 隐士等待：找一个控件，如果控件没有加载出来，等待5s中  等待所有，只需要写着一句，以后找所有控件都按这个操作来
# bro.get('https://www.baidu.com/')
#
# d_button=bro.find_element_by_link_text('登录')
#
# d_button.click()
#
# login_u=bro.find_element_by_id('TANGRAM__PSP_11__footerULoginBtn')
# login_u.click()
#
# username=bro.find_element_by_id('TANGRAM__PSP_11__userName')
# username.send_keys('yxp654799481')
# password=bro.find_element_by_id('TANGRAM__PSP_11__password')
# password.send_keys('yxp997997')
# time.sleep(3)
# submit=bro.find_element_by_id('TANGRAM__PSP_11__submit')
#
# submit.click()
# time.sleep(10)
#
# print(bro.get_cookies())
#
# bro.close()



# ##############选择器（find系列）
# ===============所有方法===================
# 1、find_element_by_id   # 通过id查找控件
# 2、find_element_by_link_text  # 通过a标签内容找
# 3、find_element_by_partial_link_text  # 通过a标签内容找，模糊匹配
# 4、find_element_by_tag_name   # 标签名
# 5、find_element_by_class_name  # 类名
# 6、find_element_by_name      # name属性
# 7、find_element_by_css_selector  # 通过css选择器
# 8、find_element_by_xpath       # 通过xpaht选择器
# 强调：

# 1、find_elements_by_xxx的形式是查找到多个元素，结果为列表




# 获取元素属性
# 重点
# tag.get_attribute('href')  # 找当前控件 的href属性对的值
# tag.text   # 获取文本内容

# 了解
# print(tag.id)   # 当前控件id号
# print(tag.location)  # 当前控件在页面位置
# print(tag.tag_name)  # 标签名
# print(tag.size)      #标签的大小



####无界面浏览器（phantomjs）
#谷歌浏览器支持不打开页面
# from selenium.webdriver.chrome.options import Options
# from selenium import webdriver
# chrome_options = Options()
# chrome_options.add_argument('window-size=1920x3000') #指定浏览器分辨率
# chrome_options.add_argument('--disable-gpu') #谷歌文档提到需要加上这个属性来规避bug
# chrome_options.add_argument('--hide-scrollbars') #隐藏滚动条, 应对一些特殊页面
# chrome_options.add_argument('blink-settings=imagesEnabled=false') #不加载图片, 提升速度
#
#
# chrome_options.add_argument('--headless') #浏览器不提供可视化页面. linux下如果系统不支持可视化不加这条会启动失败
#
#
# bro=webdriver.Chrome(chrome_options=chrome_options,executable_path='./chromedriver.exe')
# bro.get('https://www.baidu.com/')
# print(bro.page_source)
# bro.close()


######元素交互
# tag.send_keys()  # 往里面写内容
# tag.click()      # 点击控件
# tag.clear()      # 清空控件内容

#####执行js(有什么用?)

# from selenium import webdriver
# import time
# bro=webdriver.Chrome(executable_path='./chromedriver.exe')
# bro.implicitly_wait(5)  # 隐士等待：找一个控件，如果控件没有加载出来，等待5s中  等待所有，只需要写着一句，以后找所有控件都按这个操作来
# bro.get('https://www.baidu.com/')
#
#
# bro.execute_script('window.open()')
# bro.execute_script('window.open()')
# time.sleep(2)
# bro.close()


####模拟浏览器前进后退

# from selenium import webdriver
# import time
# browser=webdriver.Chrome(executable_path='./chromedriver.exe')
# browser.get('https://www.baidu.com')
# browser.get('https://www.taobao.com')
# browser.get('http://www.sina.com.cn/')
#
# browser.back()
# time.sleep(1)
# browser.forward()
#
# browser.close()


#####获取cookie
# bro.get_cookies()



#### 选项卡管理(了解)
# from selenium import webdriver
# import time
# browser=webdriver.Chrome()
# browser.get('https://www.baidu.com')
# browser.execute_script('window.open()')
#
# print(browser.window_handles) #获取所有的选项卡
# browser.switch_to_window(browser.window_handles[1])
# browser.get('https://www.taobao.com')
# time.sleep(2)
# browser.switch_to_window(browser.window_handles[0])
# browser.get('https://www.sina.com.cn')
# browser.close()



##### 异常处理
# from selenium import webdriver
# from selenium.common.exceptions import TimeoutException,NoSuchElementException,NoSuchFrameException
# browser=webdriver.Chrome()
# try:
#
#     browser.get('')
# except Exception as e:
#     print(e)
# finally:
#     # 无论是否出异常，最终都要关掉
#     browser.close()



#####动作链（）


#### 如何把屏幕拉倒最后（js控制）

# bro.execute_script('window.scrollTo(0,document.body.offsetHeight)')