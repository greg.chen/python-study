# Function to convert the output of scikit-learn's AgglomerativeClustering into the linkage matrix required by
# scipy's dendrogram function
# It takes in the model fit by AgglomerativeClustering, plus all the usual arguments of the dendrogram
# function: https://docs.scipy.org/doc/scipy-0.14.0/reference/generated/scipy.cluster.hierarchy.dendrogram.html
# Original by Mathew Kallada (BSD 3 licence), https://github.com/scikitlearn/scikit-learn/pull/3464/files
# Original computes numbers of children incorrectly
# Fixed by Derek Bridge 2017
from scipy.cluster.hierarchy import dendrogram
import numpy as np

def plot_dendrogram(model, **kwargs):
    tree_as_list = model.children_
    sizes = {}
    linkage_array = []
    start_idx = len(tree_as_list) + 1
    idx = start_idx
    for children in tree_as_list:
        linkage = []
        size = 0
        for child in children:
            linkage += [child]
            if child < start_idx:
                size += 1
            else:
                size += sizes.get(child)
        linkage += [idx - start_idx + 1, size]
        sizes[idx] = size
        idx += 1
        linkage_array += [linkage]
    dendrogram(np.array(linkage_array).astype(float), **kwargs)