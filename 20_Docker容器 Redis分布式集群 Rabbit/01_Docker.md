# 今日内容



```python
docker内容
1 docker介绍，跟传统虚拟机的比较
2 安装docker，启动
3 镜像
4 容器
5 网络
6 数据卷
7 私有仓库
8 备份回顾
9 dockerfile
10 单机容器编排 docker-compose
```

## 1 docker介绍，跟传统虚拟机的比较

```python
0 各种软件用什么语言写的
    zabbix（监控软件）：php
    ansible（批量管理主机，执行命令,无angent）：python
    openstack（云计算，管理虚拟机）：python
    jekins（ci/di）：自动化部署，java
    salstack(批量管理主机，执行命令，有angent):python
    普罗米修斯（Prometheus，监控）：go写的
    docker：go
    k8s：go


1 什么是虚拟化?
	-vmware,kvm,openstack,docker,k8s
    -云服务器：vmware,kvm虚拟化出来的
    -阿里云，腾讯云：opensatck，阿里飞天，用来管理虚拟化出来的操作系统
    -docker：进程级别的隔离，装在虚拟机，云服务器上，一个云服务上开业跑出成几百个docker容器，成百上千的服务器上，就会有个上万个容器
    -k8s：不同机器的上万个容器如何管理
    
    -虚拟化：打破实体结构间的不可切割的障碍
    
2 什么是Docker？
	- Go语言实现，开源出来，很多人用
    - docker-ce（免费），docker-ee（收费）
    - 通过go语言对lxc技术的一个封装
    -上手快，简单
3 容器与虚拟机比较
	-docker不需要虚拟硬件和操作系统，轻量级，占用体积小，启动快
    
4 Docker是一个客户端-服务器（C/S）架构程序（mysql，redis都是cs架构），整套RESTful API

5 docker非常重要的概念：镜像（image）与容器（container）
	有了镜像---》镜像运行起来是容器（真正的执行单位）
    面向对象的类     对象
6 镜像是从哪里来的？
	-镜像就是一堆文件
	-从远程仓库获取（拉取）
```

## 2 docker架构图

![1598927543254](assets/1598927543254.png)

## 3 docker安装

### 3.1 windows安装

```ptyhon
1 windows安装（不建议你装）http://get.daocloud.io/
```

### 3.2 乌班图

```python
# 0 卸载
sudo apt-get remove docker docker-engine docker.io containerd runc
# 1 安装必要工具
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
# 2 安装GPG证书
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
#换成阿里云
curl -fsSL http://mirrors.aliyun.com/docker-ce/linux/ubuntu/gpg | sudo apt-key add -
# 2 写入软件源信息
#官方
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
#换成阿里云
sudo add-apt-repository "deb [arch=amd64] http://mirrors.aliyun.com/docker-ce/linux/ubuntu $(lsb_release -cs) stable"

# 3 更新并安装docker-ce
sudo apt-get -y install docker-ce
# 4 开启docker服务
 systemctl status docker
```

### 3.3 centos上安装（必须7.0以上）

```python
# 6.8安装docker，自行搜索，升级内核版本
# docker是在linux3.1以上内核中写出来的，在乌班图下开发的，docker的新特性，乌班图先看到，
# 官方建议docker运行在centos7 以上的系统

#####安装
0 卸载
yum remove docker docker-common  docker-selinux docker-engine
rm -rf /var/lib/docker

1 更新yum
yum update
2 安装需要的软件包， yum-util 
yum install -y yum-utils device-mapper-persistent-data lvm2
3 执行（向你的yum源，增加一条记录）
yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
4 安装 
yum install docker-ce

5 验证安装（查看版本）
docker -v  
~]# docker -v  （19年03月12日发布）
# 自从分了docker-ce和docker-ee以后，以年份命名版本
Docker version 19.03.12, build 48a66213fe

6 启动docker服务
systemctl start docker

7 停止docker服务，重启docker服务
systemctl stop docker
systemctl restart docker

8 开机启动	
systemctl enable docker

9 查看概要信息
docker info（你可能写了一个管理docker的运维平台）
```

### 3.4 远程仓库

```python
# 注册，登录 https://hub.docker.com/
# 你可以上传镜像（类似于github），你把你制作的镜像传上去，别人可以下载使用

# 配置加速（之前去https://hub.docker.com/），阿里云，清华，做了一个备份，配置好以后，再拉镜像会去国内地址拉取
第一步：vi /etc/docker/daemon.json  
第二步：
{
"registry-mirrors": ["https://docker.mirrors.ustc.edu.cn"]
}
```

## 4 镜像操作

```python


-1 第一个hello-world（没有任何意义）
	docker run hello-world
	1 客户端连到服务端，服务下载了hello-world镜像
    2 通过镜像跑起容器来
    3 容器输出一些东西，通过服务端发送给客户端，客户端展示在窗口里了

0 查找镜像
	方式一：https://hub.docker.com/ 点点点搜索（你用这种）
    方式二：docker search 
1 拉取镜像 
	docker pull hello-world  最新版本latest
	docker pull hello-world:nanoserver
2 查看机器上有哪些镜像
 	docker images
    REPOSITORY  TAG   IMAGE ID   CREATED   SIZE
	镜像名字      版本   id号       创建时间   大小
3 删除镜像
	docker rmi 镜像名字/id号（# 如果还有基于这个镜像的容器，是不允许删除镜像的，先删除容器，再删除images）
    
    
    
4 练习：拉取python3.6镜像，redis最新镜像，mysql5.7镜像
    docker pull python:3.6
    docker pull redis
    docker pull mysql5.7
    docker pull centos:centos7
    
5 镜像是什么？
	-就是一堆文件，这堆文件通过docker跑起来，就是容器，你现在就把他当成，一个操作系统内跑了这个软件（centos+redis）
    -假设centos7的镜像跑起来，容器你就当成在你宿主机上跑了一个centos7的操作系统（虚拟机）
    
    
    
 6 补充（了解）
	docker image inspect 46ff56815c7c
    docker image ls
    docker image rm
    
```

## 5 容器操作

```python
1 删除容器
 docker rm 容器名字/容器id
    
2 启动并运行容器
	# 通过centos:centos7镜像，创建一个容器名字叫mycentos，并且把它运行起来
	docker run -di --name=mycentos centos:centos7
    打印出容器的id号
3 查看机器上正在运行的容器
	docker ps
4 查看宿主机上所有的容器（包括停止的）
	docker ps -a
5 停止容器
	docker stop 容器名字/id号
6 启动容器
	docker start 容器id/名字
    
7 容器的创建和启动（******）
	-创建：docker create --name=xxx redis
    docker create --name=test-container centos:centos7 ps -A
    -启动
    docker start test-container
    
    -容器一直运行的原因
    	-它有一条前台进程,一直在运行
        -以后如果自己制作的镜像，运行起容器，必须有个可以夯住的命令
        -如果该命令结束，该容器也就结束了
        
    -创建并启动（run）
    -i：表示运行容器
    -t：表示容器启动后会进入其命令行。加入这两个参数后，容器创建就能登录进去。即分配一个伪终端。
    -d：在run后面加上-d参数,则会创建一个守护式容器在后台运行（这样创建容器后不会自动登录容器，如果只加-i -t两个参数，创建后就会自动进去容器）。
    --name :为创建的容器命名。如果不写，会自动分配一个名字（英文人名）
    -v：表示目录映射关系（前者是宿主机目录，后者是容器目录，映射到宿主机上的目录），可以使用多个－v做多个目录或文件映射。注意：最好做目录映射，在宿主机上做修改，然后共享到容器上。
    -p：表示端口映射，前者是宿主机端口，后者是容器内的映射端口。可以使用多个-p做多个端口映射
    docker run -it --name=myredis redis
    	
 # 注意，docker run  镜像 如果本地没有，会先pull，再run

8 进入容器的几种方式（并不是真进入）
	第一种：docker exec -it  容器id /bin/bash
    	docker exec 容器id ls 
        exec真正的作用是在容器内执行命令
    第二种：ssh连接（容器内部装ssh服务端）
9 退出
	exit
    
10 文件拷贝
	-从宿主机拷贝到容器内部
    	docker cp lqz.txt 容器id:/home
    -从容器内部拷贝到宿主机
    	docker cp 容器id:/home/lqz.txt /app
11 目录挂载	
	docker run -di --name=mycentos99 -v /home/lqz:/home centos:centos7
    # 一旦挂载，以后宿主机目录内修改，同样影响容器内部
12 端口映射
	docker run -di -p 6377:6379 redis:latest
    
    # 了解：/usr/local/bin/docker-entrypoint.sh
    
13 查看容器详细信息
	docker inspect f81
    # 查看ip地址
    docker inspect --format='{{.NetworkSettings.IPAddress}}' 容器名称（容器ID）
```

## 6 应用部署

```python
# 在容器内部署mysql
# -e表示环境变量

docker run -di --name=mysql -p 3307:3306 -e MYSQL_ROOT_PASSWORD=123456 mysql:5.7
# 官方提供的
docker run --name some-mysql -v /my/own/datadir:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:tag
        
# 在容器内部署redis
docker run -di -p 6377:6379 redis:latest
        
# 在容器内部署nginx
docker run -di --name=mynginx -p 8080:80 nginx
```



## 作业

1 讲的东西练习一下

2 纯净的centos7镜像---》容器---》装python3.6

3 5道力扣题

