# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    l = [10, 3.1, 'aaa', ['vbbbb', 'cccc'], 'dddd']
    print(l[3])
    print(l[-1])
    d = {'name': 'jingjing', 'age': 38}
    print('Hi, %s, %s' % (d['name'], d['age']));

    kwargs = '我的名字{name}, age:{age}'.format(name="jingjing", age=18)
    print(kwargs)
    name = '大黑牛'
    res = f'我的名字{name}'
    print(res)

    age = 18
    if age > 16 and age < 20:
        print(age)

    score = int(input("请输入成绩"))
    if score >=90:
        print('优秀')
    elif score >=80:
        print('良好')
    elif score >= 70:
        print('普通')
    else:
        print('及格')

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
