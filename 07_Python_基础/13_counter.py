from collections import Counter

color_list=['red','blue','red', 'green','blue']

cn2 = Counter(color_list);
print(cn2)
print(cn2['red'])

# 参数方式构建
print(Counter(cats=4, dogs=8))
print(Counter(dict((['eggs', 2],['ham',1]))))


c=Counter({'ham': 1, 'eggs': 2, 'apple': 1,'banana':-1})
b=Counter({'orange':0})
print (c+b)

c = Counter({'eggs', 'ham', 'eggs', 'apple'})
print(c)