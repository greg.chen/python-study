# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.
    res = str({'a':1})
    print(res, type(res))

    msg = 'hello world'
    print(msg[0])
    print(msg[-1])

    #切片
    print(msg[0:5]) #顾头不顾尾
    # 切片步长
    print(msg[0:5:2])  # 0 2 4

    #in
    print("hello" in "hello world")
    print("hello" not in "hello world")

    #strip() 方法用于移除字符串头尾指定的字符（默认为空格或换行符
    msg = "   hello   hello    hello  "
    print(msg.strip())
    print(msg) #strip不会改变原值

    #切分split
    msg = "1234;12341324"
    print(msg.split(";"))

    #lower upper
    msg = "   hello   hello    hello  "
    print(msg.lower())
    print(msg.upper())

    #startswith endswith
    msg = "hello   hello    hello"
    print(msg.startswith("h"))
    print(msg.endswith("h"))

    # join 把列表拼接成字符串
    l =["hello", "world"]
    print(" ".join(l))

    #replace
    msg = "cello"
    print(msg.replace("c", "h"))

    #isdigit
    print("123".isdigit())

    input = ['Jan', 'Feb', 'March']
    print(' - '.join([newStr.upper() for newStr in input]))

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
