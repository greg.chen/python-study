# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    res = {'k1':111, 'k2':2222, 'k4':44444, 'k5':5555, 'k6':66666}

    for x in res:
        print(x, end=' ')

    print('')

    res['k3'] = 333
    print(res)

    #pop 根据key删除
    res.pop('k3')
    print(res)

    #popitem 返回并删除字典中的最后一对键和值
    res.popitem()
    print(res)

    for k in res.keys():
        print(k)

    print(res.values())
    print(res.items())
    #元组
    for item in res.items():
        print(item)

    for k,v in res.items():
        print(k, v)

    #update
    res.update({'k1':11111111111, 'k2':2222222222, 'k7':777})
    print(res)

    #get 不存在返回None
    print(res.get('k7'))
    print(res.get('k8'))

    #setdefault 如果键不存在于字典中，将会添加键并将值设为默认值。
    res.setdefault('k9',99999)
    print(res)

    if 'k1' in res.keys():
        print("k1 exists")



    #clear
    res.clear()
    print(res)

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

    user_info = {"name": "悟空", "age": 100, "gender": "male", "job": "取经", "job": "偷桃"}
    print(user_info["job"])
    print("len",len(user_info.keys()))

    s1 = 'a'
    s2 = 'b'
    d = {s1: 1, s2: 2}
    d['a'] = 99
    print(d)

    d = {1: 1, 2: 2}
    print(d[1])
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
