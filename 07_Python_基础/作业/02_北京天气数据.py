import csv

file = "BeijingPM20100101_20151231.csv"

# 使用DictReader读取csv内容，并打印出csv文件第一行所标识的列名
f = open(file, 'r')
reader = csv.DictReader(f)
print(reader.fieldnames)

pm_name_list = ['PM_Dongsi', 'PM_Dongsihuan', 'PM_Nongzhanguan', 'PM_US Post']
pm_average = []
iws = []
for row in reader:
    # 提取row中的PM相关列，计算均值放入到pm_average列表中
    # 提取Iws列，放入到iws列表中
    pm_average_sum = 0
    pm_average_count= 0;
    for pm_name in pm_name_list:
        if row[pm_name] == 'NA':
            continue
        else:
            pm_average_sum += int(row[pm_name])
            pm_average_count +=1

    if pm_average_sum != 0:
        pm_average_item = pm_average_sum / pm_average_count
        pm_average.append(pm_average_item)
    else:
        pm_average.append('NA')

    iws.append(row['Iws'])
# 关闭文件
f.close()

# 打印iws的前3个元素

# TODO
# 遍历pm_average，将值为'NA'的元素删除，并将iws中相应位置的元素也删除（留意在遍历过程中删列表元素可能会遇到问题）
for index,item in enumerate(pm_average):
    if item == 'NA':
        iws.pop(index)

for index,item in enumerate(iws):
    if item == 'NA':
        pm_average.pop(index)
# TODO
# 遍历iws，将值为'NA'的元素删除，并将pm_average中相应位置的元素也删除
iws = [x for x in iws if x !='NA']
pm_average = [x for x in pm_average if x !='NA']

# 打印去除无效值之后iws以及pm_average的列表长度
print("iws len",len(iws))
print("pm_average len",len(pm_average))


iws = [float(x) for x in iws if x !='NA']
# 打印iws的前3个元素
print("iws",iws[:30])
print("pm_average",pm_average[:30])

iws_sum = sum(iws)
pm_sum = sum(pm_average)

print("iws_sum",iws_sum)
print("pm_sum",pm_sum)
# TODO
# 计算pm_average以及iws列表的元素和，分别存入变量pm_sum和iws_sum中
import math

iws_mean =  iws_sum/ len(iws)
pm_mean = pm_sum / len(pm_average)

# 打印iws以及pm的均值
print('Iws mean is %f'%iws_mean)
print('pm mean is %f'%pm_mean)

pm_var_sum = sum([math.pow(x-pm_mean,2) for x in pm_average if x !='0'])
iws_var_sum = sum([math.pow(x-iws_mean,2) for x in iws if x !='0'])

iws_std = math.sqrt(iws_var_sum / len(iws) - 1)
pm_std = math.sqrt(pm_var_sum / len(pm_average) - 1)

# 打印iws_std以及pm_std值
print('Iws std is %f'%iws_std)
print('pm std is %f'%pm_std)

# TODO
# 使用函数z_score，根据输入的列表data,列表的均值mean，列表的标准差std计算z-score，并将结果列表返回
def z_score(data, mean, std):
    return [(x-mean)/std for x in data];

# 调用z_score函数分别计算pm以及iws的z-score
pm_z_score = z_score(pm_average, pm_mean, pm_std)
iws_z_score = z_score(iws, iws_mean, iws_std)

# 打印pm以及iws的z-score列表的前三个元素
print(pm_z_score[:3])
print(iws_z_score[:3])

# 导入matplotlib的绘图包
import matplotlib.pyplot as plt

# TODO
# 使用matplotlib绘制散点图，将pm_z_score作为x坐标，iws_z_score作为y坐标

plt.scatter(pm_z_score, iws_z_score)
plt.show()