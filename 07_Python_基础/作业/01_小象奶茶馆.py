# 定义奶茶商品
milktea_dict = {'1': {'name': '原味冰奶茶', 'price': 3},
                '2': {'name': '香蕉冰奶茶', 'price': 5},
                '3': {'name': '草莓冰奶茶', 'price': 5},
                '4': {'name': '蒟蒻冰奶茶', 'price': 7},
                '5': {'name': '珍珠冰奶茶', 'price': 7},
                }


def shopping_procedure():
    '''获取用户的购买需求输入

    返回值：
        字典类型goods_dic，key为所购奶茶的编号，value为编号所对应的奶茶的购买数量。

    '''

    # 定义返回字典变量
    goods_dic = {}

    while True:
        # 获取用户所要购买的奶茶编号
        milktea_no = input('请输入您要购买的奶茶编号，完成购物请选择Q：')

        # TODO
        # 如果输入为‘Q’，退出购买流程
        if milktea_no.upper() == 'Q':
            break;

        # TODO，修改if语句条件
        # 如果输入的milktea_no编号在有效范围内
        if milktea_dict.__contains__(milktea_no):
            # 提示用户输入购买数量
            milktea_amount = input('请输入您要购买的数量：')
            if not goods_dic.__contains__(milktea_no):
            # TODO
            # 向goods_dic中加入新项，key为milktea_no，value为milktea_amount
                goods_dic[milktea_no] = int(milktea_amount)
            else:
                # 根据输入的milktea_no及milktea_amount，将相应的goods_dic项目元素做累加操作
                goods_dic[milktea_no] = goods_dic[milktea_no] + int(milktea_amount)
        else:
            print('我们只售卖以上五种奶茶哦！新口味敬请期待！')

        print('您还需要其它口味吗？')

    return goods_dic


def receipt_print(goods_dic):
    '''打印订单详情并计算总金额

    参数：
        goods_dic：字典类型，key为购买的奶茶类型编号，value为购买的数量

    返回值：
        订单总金额。

    '''
    print('点单完成！您的购买详情为：')

    total_money = 0
    for milktea_no, milktea_amount in goods_dic.items():
        print('{}号奶茶:{}杯'.format(milktea_no, milktea_amount))

        # TODO
        # 将购买milktea_amount数量的milktea_no奶茶所需要的金额汇总到total_money变量
        total_money += milktea_dict[milktea_no]['price'] * milktea_amount

    print('您的总消费额为：{}元'.format(total_money))

    # 返回金额总数
    return total_money


def shopping_log(goods_dic, customer_no, total_consumer_record):
    '''记录购买行为

    参数：
        goods_dic：字典类型，key为购买的奶茶类型编号，value为购买的数量
        customer_no：用户编号
        total_consumer_record：总体购买记录，列表类型，其中每个元素为一次购买记录。
                               元素为字典类型，字典由三个key-value对组成，分别记录某一个奶茶订单的信息：
                               用户id（customer_no），奶茶编号（milktea_no）,购买数量（milktea_amount）。

    返回值：
        总体购买记录列表。

    '''
    for milktea_no, milktea_amount in goods_dic.items():
        single_consumer_record = {}
        single_consumer_record['customer_no'] = customer_no
        single_consumer_record['milktea_no'] = milktea_no
        single_consumer_record['milktea_amount'] = milktea_amount

        # TODO
        # 将single_sonsumer_record项添加到total_consumer_record中
        total_consumer_record.append(single_consumer_record)

    return total_consumer_record


total_consumer_record = []
# vip会员记录
vip_dic = {}

i = 1
while True:
    print('\n欢迎光临小象奶茶馆！本店售卖宇宙无敌奶茶，奶茶虽好，可不要贪杯哦！\n 1）原味冰奶茶 3元  2）香蕉冰奶茶 5元 '
          ' 3) 草莓冰奶茶 5元  4）蒟蒻冰奶茶 7元  5）珍珠冰奶茶 7元')
    print('本店每日接待5位顾客，您是今天第{}位幸运儿'.format(i))

    # 调用shopping_produre进行商品选择
    goods_dic = shopping_procedure()

    # TODO
    # 若此次没有做商品选择，跳过后续计算费用环节继续下个循环
    if len(goods_dic.keys()) == 0:
        continue

    # 调用receipt_print计算费用总额
    vip_no = input('请输入您的专属会员号(新会员直接设置会员号即可，激活手机号方可享受会员价）：')
    total_money = receipt_print(goods_dic)

    # TODO
    # 如果vip_no在vip_dic中能够查到，将总价打9折，并显示'您可以享受会员价，折后总价：xx元'，其中xx为计算得到的折后价格。
    # 否则，打印'请输入您的手机号激活会员：'提示用户输入电话号码。将非会员添加到会员表vip_dic中，
    # 新添项的key是vip_no，value是用户输入的电话号码。
    if vip_dic.__contains__(vip_no):
        total_money = total_money * 0.9
        print('您可以享受会员价，折后总价:%s元' % total_money)
    else:
        phone = input('请输入您的手机号激活会员：')
        vip_dic[vip_no] = phone

    # 调用shopping_log记录用户购买动作
    total_consumer_record = shopping_log(goods_dic, vip_no, total_consumer_record)

    print("\n********************************************************")
    print('\t小象奶茶馆力争做一枚有态度、有思想的奶茶馆（傲娇脸）！\n\t祝您今日购物愉快！诚挚欢迎您再次光临！')
    print("********************************************************")

    # TODO
    # 当购买次数大于等于5次时，打印'今日已闭店，欢迎您明天光临！'并退出购买循环

    i += 1

# 打印今天的所有销售情况
print('今日售卖情况:{}'.format(total_consumer_record))
