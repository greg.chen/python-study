# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.
    l = ['jingjing', 'jingjing1', 'jingjing2', 'jingjing3']
    for x in l:
        print(x)

    i =0
    while i < len(l):
        print(l[i])
        i +=1

    dic={'k1':111, 'k2':222, 'k3':333}
    for k in dic:
        print('k:{k}, v:{v}'.format(k=k, v=dic[k]))

    msg="jingjing jijijijij"
    for x in msg:
        print(x)

    for x in [1,2,3]:
        print(x)

    for x in range(1,9): #1...8
        print(x)

    for i in range(3):
        print("外层", i)
        for j in range(5):
            print("内层", j)



# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

    ## 体重列表
    weight = [67, 65, 67.0, 48, 51, 60]
    ## 名字列表
    name = ['Robi', 'Mike', 'Ken', 'Rose', 'Jenny', 'Jano']

    ## 构造体重与名字的对应关系
    dict = {}
    for i in range(len(weight)):
        dict[str(weight[i])] = name[i]


    ## 打印体重大于60的人
    for k, v in dict.items():
        if (float(k) > 60):
            print(v)
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
