
# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.




def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.



    #以t模式为基准
    # r 只读模式. 当文件不存在时会报错
    with open('a/user.txt', mode='rt', encoding='utf-8') as f:
        # 操作文件
        for line in f:
            l = line.strip().split(":")
            print(l)
        # username, password = res.split(":")
        # print(username, password)
        #print(res)

    # w 只能写, 不能读. 文件不存在, 创建; 文件存在, 清空
    with open('a/f.txt', mode='wt', encoding='utf-8') as f:
        f.write("hahahahah\n")
        f.write("hahahahah\n")
        f.write("hahahahah\n")

    # a 只追加(不能读), 文件不存在, 创建空文档. 文件存在, 文件指针跳到末尾
    with open('a/f.txt', mode='at', encoding='utf-8') as f:
        f.write("hahahahah\n")
        f.write("hahahahah\n")
        f.write("hahahahah\n")









# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
