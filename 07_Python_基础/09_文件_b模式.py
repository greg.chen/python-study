# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.




def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    res = 0
    if not res:
        print("xxx")

    #b模式为基准
    # 读写二进制

    # w 只能写, 不能读. 文件不存在, 创建; 文件存在, 清空
    with open('a/f.txt', mode='rb') as f:
        res = f.read()
        print(res, type(res))
        print(res.decode("utf-8"))

    with open('a/e.txt', mode='wb') as f:
        f.write('你好'.encode('utf-8'))


    with open('a/user.txt', mode="rb") as f1, \
        open('a/z.txt', mode="wb") as f2 :
            while True:
                res = f1.read(1024)  #当一行内容过长时, 选用这种模式
                if len(res) == 0:
                    break
                print(len(res))
                f2.write(res)











# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
