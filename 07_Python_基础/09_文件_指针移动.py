# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.




def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.




    with open('a/d.txt', mode="rt", encoding="utf-8") as f1:
        #读取4个字符
        res = f1.read(4)
        print(res)


    with open('a/d.txt', mode="rb") as f1:
        # 移动字节个数
        #0代表从文件开头开始算起，1代表从当前位置开始算起，2代表从文件末尾算起。
        # 模式默认0, 参照物文件开头位置
        f1.seek(1)
        # 读取4个字节
        res = f1.read(1)
        print(res)
        # 模式1, 当前位置移动
        f1.seek(1, 1)
        res = f1.read(1)
        print(res)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
