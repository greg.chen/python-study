# 将可迭代对象，打包成由对象中的对应元素组成的元组列表（Python2）或者Zip对象（Python3）
#
# 语法
# zip([iterable, ...])
#
# 注意
# 1. 当各个迭代器的元素个数不一致时，则返回列表长度与最短的对象相同。
# 2. 使用”*”做解压操作。

a = [1,2,3]
b = [4,5,6]
c = [7,8,9]

zipped = zip(a,b)
zipped_List = list(zipped)
print(zipped_List)
print(dict(zip(a,c)))

#解压
unzip = zip(*zipped_List)
print(list(unzip))

name =['Tom', 'Jerry', 'Dora']
weight = [15,5,30]
zipped = zip(name, weight)

for k,v in zipped:
    print("%s, weight is %d" %(k,v))