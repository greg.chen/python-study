# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.
    # 元组只读, 不可修改
    t = (1,2,3,4) #如果元组只有一个元素必须加,
    print(t)

    #类型转换
    print(tuple('hello'))
    print(tuple([1,2,3,4]))
    print(tuple({'a1':111, 'a2':2333}))

    t = (1,2,3,4)
    print( 1 in t)
    print(t[:-1])

    for x in t:
        print(x)

    #倒序
    tuple1 = (0, 1, 2, 3)
    print(tuple1[::-1])

    print((1, 2) + (3, 4))

    a = (1, 2)
    b = (3, 4)
    c = a + b
    print((id)(a) == (id)(c))



# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
