# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    res = list('hello')
    print(res)

    res = list({'k1':111, 'k2':2222})
    print(res)

    #追加
    l = ['111','222','333']
    l.append('4444')
    print(l)

    #插值
    l = ['111','222','333']
    l.insert(0, '0000')
    print(l)

    #extend
    l = ['111', '222', '333']
    l2 = ['444', '555', '666']
    l.extend(l2)
    print(l)

    #删除
    l = ['111', '222', '333']
    rs = l.pop(0) #返回删除的值
    print(l)
    print(rs)

    #根据元素删除
    l.remove('222')
    print(l)

    ## 切片
    l = ['111', '222', '333', '444', '555', '666']
    print(l[0:])
    print(l[2:5])

    print('111' in l)
    print(l.count('111'))

    list1 = ['I', 'like', 'machine', 'learning']
    del (list1[list1.index('like')])
    print(list1)

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
