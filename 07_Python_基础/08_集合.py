# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    # 元素无序
    # 元素没有重复值
    # 无序存储不同数据类型不重复元素的序列
    s = {1,2,3}

    s1 = {1,1,1,1,1,1,2,3,4,5,6, 'a', 'b', 'c', 'd', 'e'}
    for i in s1:
        print(i)
    s2 = {2,3,4,5,6,7}
    print(s1)
    #交集
    res = s1 & s2
    print(f'交集:{res}')
    #合集
    res = s1 | s2
    print(f'合集:{res}')

    #差集
    res = s1 - s2
    print(f'差集:{res}')

    # 对称差集
    res = (s1 -s2) | (s2 - s1)
    print(f'对称差集:{res}')
    #包含
    print(s1> s)
    print(s1< s)

    #删除
    s1.discard(1) #remove 删除元素不存在会报错
    s1.remove(2)
    print(s1)

    #更新
    s1.update({1,3,5,9,0,0})
    print(s1)

    #pop 随机删
    s1.pop()
    s1.pop()
    s1.pop()
    print(s1)

def return_set():
    return {2, 2, 3, 1}



# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')
    a, b, c = return_set()
    print(return_set())

    box = set(range((5)))
    print(box)
    while True:
        try:
            input('输入任意字符，从集合中取出一个元素并输出：')
            print(box.pop())
        except:
            print('所有元素已经取完！')
            exit()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
