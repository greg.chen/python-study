# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

# 在程序运行过程中, 可以动态获得对象的信息

class People:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def say(self):
        print("<%s:%s>" %(self.name, self.age))

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

    obj = People('JJ',35)
    getattr(obj,'say')()
    print(hasattr(obj, 'name'))
    print(hasattr(obj, 'age'))

    setattr(obj, 'name','Greg')
    obj.say()

    delattr(obj,'name')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
