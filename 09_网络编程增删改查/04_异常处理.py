# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.






def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

    try:
        #a = 1/0
        l=['aaaa','bbbb']
        #l[3]

    except IndexError as e:
        print('IndexError: ', e)
    except NameError as e:
        print('NameError: ', e)
    except Exception as e:
        print('Exception: ', e)
    else:
        print("=====>")
    finally:
        print("finally")
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
