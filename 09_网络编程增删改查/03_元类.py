# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

# 元类用来实例化产生类的类
# 元类-->实例化-->类-->实例化-->对象
class Mymeta(type):
    def __init__(self,x,y,z):
        print('run __init__....',x,y,z)

        if not x.istitle():
            raise NameError('首字母必须大些');

    def __new__(cls, *args, **kwargs):
        print('run __new__')
        print(args)
        print(kwargs)
        return  type.__new__(cls, *args, **kwargs)

    def __call__(self, *args, **kwargs):
        print("run __call___", args, kwargs)

        people_obj = self.__new__(self)
        self.__init__(people_obj, *args, **kwargs)

        return people_obj


class People(metaclass=Mymeta):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def say(self):
        print("<%s:%s>" %(self.name, self.age))


    def __str__(self):
        return "<%s:%s>" % (self.name, self.age)





def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

    # 默认元类type
    #print(type(People))

    obj = People('JJ',35)
    print(obj)



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
