import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

'''
类别散布图

1.sns.stripplot() 分布散点图

2.sns.swarmplot() 分簇散点图

类别内数据分布

1.sns.boxplot() 盒子图

四分位距：IQR=Q3-Q1

最大值：Q3+1.5*IQR，最小值：Q1-1.5*IQR

2.sns.violinplot() 小提琴图

类别内统计图

sns.barplot() 柱状图

sns.pointplot() 点图
'''

# 加载小费数据集
tips_data = sns.load_dataset('tips')
print(tips_data.head())

sns.stripplot(x='tip', y='day', data=tips_data)

# 类别散布图
sns.swarmplot(x='tip', y='day', data=tips_data, hue='sex')

# 盒子图
sns.boxplot(x='day', y='tip', data=tips_data)

# 盒子图
sns.boxplot(x='day', y='tip', data=tips_data, hue='sex')