
# 引入包
import matplotlib.pyplot as plt
import numpy as np


'''
矩阵绘图通过可视化的方式呈现矩阵数据，可用于显示三维信息

plt.imshow(X, cmap)

X 矩阵数据（二维数组）

cmap 颜色主题
https://matplotlib.org/users/colormaps.html

plt.colorbar() 为图像添加颜色条

'''

# 准备数据
m = np.random.rand(10, 12)
print(m)
plt.imshow(m)
plt.colorbar()
plt.show()

# 使用颜色主题
plt.imshow(m, cmap=plt.cm.gnuplot)
plt.colorbar()
plt.show()

# 使用颜色主题
plt.imshow(m, cmap=plt.cm.ocean)
plt.colorbar()
plt.show()