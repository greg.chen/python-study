from sklearn import datasets
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


titanic_data = sns.load_dataset("titanic")
sns.swarmplot(x="sex",y="fare",data=titanic_data,hue="survived",dodge=True)
plt.show()