from sklearn import datasets
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

iris_data = datasets.load_iris()
print(iris_data['feature_names']+['class'])
print(iris_data['target'])

data1 = iris_data["data"]
data2 = iris_data["target"].reshape(150,1)
data=pd.DataFrame(np.concatenate((data1,data2),axis=1),columns=iris_data['feature_names']+['class'])
print(data)

sns.pairplot(data,x_vars=["sepal length (cm)","sepal width (cm)"],
             y_vars=["sepal width (cm)","sepal length (cm)"],hue="class", markers=["o", "s", "D"])

sns.pairplot(data,vars=['petal length (cm)','petal width (cm)'],hue="class",markers=["o", "s", "D"],palette="husl")


plt.show()