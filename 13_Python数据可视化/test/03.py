from sklearn import datasets
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


tips_data = sns.load_dataset("tips")

box_obj = tips_data.plot(kind="box",y=['total_bill','tip'],grid=True,fontsize=20,title='total_bill and tip',figsize=(10,5))

plt.show()