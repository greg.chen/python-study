import matplotlib.pyplot as plt
import numpy as np
import matplotlib

'''
设置刻度范围

plt.xlim(), plt.ylim()

ax.set_xlim(), ax.set_ylim()

设置显示的刻度

plt.xticks(), plt.yticks()

ax.set_xticks(), ax.set_yticks()

设置刻度标签

如果使用plt设置刻度标签，需要使用 plt.xticks(), plt.yticks()进行配置

ax.set_xticklabels(), ax.set_yticklabels()

设置坐标轴标签

ax.set_xlabel(), ax.set_ylabel()

设置标题

plt.title()

ax.set_title()

图例

绘制图形时，使用label参数

ax.legend(loc), plt.legend(loc)

loc=‘best’自动选择放置图例最佳位置

中文显示

使用rcParams属性进行配置

a.指定默认字体

matplotlib.rcParams['font.sans-serif'] = ['SimHei’]

b.解决保存图像是负号'-'显示为方块的问题

matplotlib.rcParams['axes.unicode_minus'] = False
'''

# 生成数据
data1 = np.random.randn(1000).cumsum()
data2 = np.random.randn(1000).cumsum()
data3 = np.random.randn(1000).cumsum()

fig, ax = plt.subplots(1)

# 设置刻度范围
ax.set_xlim([0,800])
# 设置显示的刻度
ax.set_xticks([0, 100, 200, 300, 400, 500])
# 设置刻度标签
ax.set_xticklabels(['x1', 'x2', 'x3', 'x4', 'x5','x6'])
# 设置坐标轴标签
ax.set_xlabel('Number')
ax.set_ylabel('Random')
# 设置标题
ax.set_title('Example')


ax.plot(data1,label='line1')
ax.plot(data2, label='line2')
ax.plot(data3, label='line3')
ax.legend(loc='best')
plt.show()

#中文显示
matplotlib.rcParams['font.sans-serif'] = ['SimHei']  # 指定默认字体
matplotlib.rcParams['axes.unicode_minus'] = False


fig, ax = plt.subplots(1)
ax.plot(data1, label='线1')
ax.plot(data2, label='线2')
ax.plot(data3, label='线3')

# 设置刻度
ax.set_xlim([0, 800])

# 设置显示的刻度
ax.set_xticks([0, 100, 200, 300, 400, 500])

# 设置刻度标签
ax.set_xticklabels(['一', '二', '三', '四', '五','六'])

# 设置坐标轴标签
ax.set_xlabel('数据个数')
ax.set_ylabel('随机数')

# 设置标题
ax.set_title('示例3')

# 图例
ax.legend(loc='best')
plt.show()

