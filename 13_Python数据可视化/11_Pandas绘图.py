import pandas as pd
import matplotlib.pyplot as plt

'''
Series或DataFrame对象都可通过 plot(x, y, kind)  进行绘图

如果不指定 x,y，默认行索引为x轴

kind可以为

1>line, 默认，折线图

2>bar, barh 柱状图，

多组数据时，默认为分组柱状图

指定stacked=True绘制堆叠柱状图

3>hist 直方图

bins 桶的个数

多组数据是，stacked=True绘制堆叠直方图

4>box 盒子图

5>area 面积图

6>scatter 散点图

7>pie 饼状图

需要指定figsize的长宽一致才能显示圆形

autopct指定显示百分比，如autopct=‘%.2f%%’ à 33.33%

'''

# 读取数据
iris_data = pd.read_csv('./dataset/iris.csv')
iris_data.head()

print(iris_data.groupby('species').mean())

#折线图
iris_data.plot()
# 分组柱状图
iris_data.groupby('species').mean().plot(kind='bar')
# 堆叠柱状图
iris_data.groupby('species').mean().plot(kind='bar', stacked=True)
#盒子图
iris_data.plot(kind='box')
#面积图
iris_data.plot(kind='area', alpha=0.6)
#散点图
iris_data.plot(x='sepal_length', y='sepal_width', kind='scatter')
#饼状图
iris_data.groupby('species').size().plot(kind='pie', autopct='%.2f%%')

count_ser = iris_data.groupby('species').size()
count_ser.name = 'Count'
ax = count_ser.plot(kind='pie', autopct='%.2f%%', figsize=(6, 6))
# 避免标签重叠
ax.yaxis.labelpad = 30

plt.show()

