import matplotlib.pyplot as plt
import numpy as np


'''
plt.plot(x, y, fmt) 绘制折线图

x, y 横轴纵轴数据

fmt 颜色 标记 线型的样式

如 r.-- 表示 红色 点标记 虚线
'''

fig, subplot_arr = plt.subplots(2, 2, figsize=(8, 8))

data = np.random.randn(20)

subplot_arr[0, 0].plot(data, '--r.')
subplot_arr[0, 1].plot(data, 'gv:')
subplot_arr[1, 0].plot(data, 'b<-')
subplot_arr[1, 1].plot(data, 'ys-.')

plt.show()