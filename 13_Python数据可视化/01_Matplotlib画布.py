import matplotlib.pyplot as plt
import numpy as np
import pylab as pl

'''

Matplotlib绘图步骤：

a.创建画布/图纸 (figure)

b.在画布上创建一个或多个绘图 (plotting) 区域 (坐标系/轴，axis)

c.在plotting区域上描绘点、线等

d.为plotting （绘图线或坐标轴）添加修饰标签

e.其他各种DIY（如：旋转标签、添加图例）

Matplotlib基本元素：

a.变量

b.函数

c.画布 (figure) 和坐标轴 (axes)

'''

'''
1.Matplotlib的图像位于figure对象中

2.通过 plt.figure() 创建

1>如果没有指定创建，matplotlib会自动生成默认的画布

2>参数 figsize 控制画布大小，单位为英寸（inch），1 inch = 2.54 cm

3. plt.show() 显示绘图结果

注意，在jupyter notebook中使用matplotlib时，代码块要放在一个cell中

'''

# 创建画布
plt.figure()

# 生成数据
data = [2, 3, 6, 7, 11]

# 可视化结果
plt.plot(data)
plt.show()

# 创建画布
plt.figure(figsize=(20, 5))
# 生成数据
random_data = np.random.randn(100)

plt.plot(random_data)
plt.show()
