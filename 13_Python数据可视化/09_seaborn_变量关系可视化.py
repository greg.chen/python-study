import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

'''
多变量关系可视化：sns.pairplot(data, hue, vars, kind, diag_kind)

data: DataFrame数据集，列为变量，行为样本

hue：数据集中作为类别的列名

vars：可视化的列（默认可视化所有列间的关系）

kind：scatter 散点，reg 添加拟合线

diag_kind：对角线的图像，hist 直方图，kde 核密度估计图
'''

# 加载鸢尾花数据集
data = pd.read_csv('./dataset/iris.csv')
print(data.head())

sns.pairplot(data)

# 加入类别
sns.pairplot(data, hue='species')

# 选择部分变量
sns.pairplot(data, hue='species', vars=['sepal_length', 'sepal_width'])

# 加入拟合线
sns.pairplot(data, hue='species', vars=['sepal_length', 'sepal_width'], kind='reg')

# 对角线为核密度估计图
sns.pairplot(data, hue='species', vars=['sepal_length', 'sepal_width'], kind='reg', diag_kind='kde')


plt.show()