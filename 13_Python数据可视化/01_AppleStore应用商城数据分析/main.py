import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

'''
### 项目介绍：
* 随着网络的普及，各行各业都呈现蓬勃向上的发展趋势，而智能手机是改变我们生活的重要方式之一，
因为智能手机可以允许安装各种app应用，这些应用极大的便利了我们的生活，因此，对app应用程序进行分析变得极为重要。
* 现在我们要对具有代表性的苹果应用商城的app应用进行分析，根据每个app应用的属性进行相关性探索。

数据集分析¶
项目AppleStore数据集包含应用程序ID、名称、大小、价格、评分、内容评级、主要类型、支持设备类型数量等信息，
共包括7207行

字段的描述¶
这个项目我们将对以下字段进行相关性分析。
size_bytes： 大小（以字节为单位）
price： 价格金额
rating_count_tot： 用户评分计数（适用于所有版本）
rating_count_ver：用户评分计数（当前版本）
user_rating：平均用户评分值（适用于所有版本），取值范围为(0,5]，左开右闭区间
user_rating_ver：平均用户评分值（对于当前版本）取值范围(0,5]，左开右闭区间
prime_genre：主要类型
sup_devices.num：支持设备的数量
lang.num：支持的语言数量

分析流程：
查看数据：导入数据，了解数据集的维度，每列数据的数据类型，以及打印部分数据，进行观察
数据清洗：删除除不需要的列，删除重复行数据，删除包含有空值的行数据，以及对异常值进行处理
分析数据：
(1)：对用户评分进行分析
(2)：对prime_genre数据分析
(3)：发散题（选做，对评优没有影响）

'''
filepath = './dataFile/AppleStore.csv'
data_all = pd.read_csv(filepath,index_col=0)

# TODO
# 使用info打印数据集每列的信息
print('数据集中每列的信息：', data_all.info())


def drop_col(data_all, del_col):
    """
    功能：
        判断data_all数据集中是否存在del_col中的列，若是存在，则删除该列数据
    参数：
        data_all是DataFrame结构的数据集，del_col是一个列表，包含了要删除的列，需要inplace原数据集删除
    """
    # TODO
    # 判断del_col中的列是否在data_all数据集中，若是存在，则删除

    if set(del_col).issubset(data_all.columns):
            data_all.drop(columns=del_col, inplace=True)

del_col =['id','track_name','cont_rating','currency','ver','ipadSc_urls.num','vpp_lic']
drop_col(data_all,del_col)
print(data_all.head())


def drop_duplicat_row(data_all):
    """
    功能：
        原数据集删除重复行
    参数：
        data_all是DataFrame的数据集，也是本项目所用的数据集

    """
    # TODO
    # 判断data_all是否存在重复行，若是存在，则打印"存在重复行"，同时，原数据集删除重复行，且保留最后一个重复行

    if data_all.duplicated().any():
        print("重复行",data_all[data_all.duplicated()])
        data_all.drop_duplicates(inplace=True)

print('去除重复行前，数据集的维度是：{}'.format(data_all.shape))
drop_duplicat_row(data_all)
print('去除重复行后，数据集的维度是：{}'.format(data_all.shape))


def drop_nan(data_all):
    """
    功能：
        原数据集删除缺失值
    参数：
        data_all是DataFrame的数据集，也是本项目所用的数据集

    """
    # TODO
    # 计算含有空值的行的个数，并且打印出行数，然后若是存在含有空值的行，则本地删除含有空值的行数据
    print("含有空值的行", data_all[data_all.isnull().T.any()])
    if len(data_all[data_all.isnull().T.any()]) > 0:
        data_all.dropna(inplace=True)

drop_nan(data_all)


print(data_all.describe())
# TODO
# 将列名为user_rating和user_rating_ver的列数据中所有大于5的数据设定为5。将列数据中评分为0的异常值设定为1.
# 要求在原数据集设定新评分值

data_all.loc[data_all['user_rating'] > 5] = 5
data_all.loc[data_all['user_rating_ver'] > 5] = 5
data_all.loc[data_all['user_rating'] < 1] = 1
data_all.loc[data_all['user_rating_ver'] < 1] = 1

ele_lst = ['user_rating','user_rating_ver']
print(data_all.loc[:,ele_lst].describe())

'''
变量相关性分析
rating_count_tot： 用户评分计数（适用于所有版本）
rating_count_ver：用户评分计数（当前版本）
user_rating：平均用户评分值（适用于所有版本），取值范围为(0,5]，左开右闭区间
user_rating_ver：平均用户评分值（对于当前版本）取值范围(0,5]，左开右闭区间

依据user_rating，对rating_count_ver、rating_count_tot、user_rating_ver、user_rating进行相关性分析，了解这四个列存在的关系
'''
# 了解用户评分列数据中具体包含的非重复星级评分
print(data_all['user_rating'].unique())


def user_rating_group(ele):
    """
    功能：
        对用户评分user_rating的数据进行类别划分
    参数：
        ele是用户评分的每个元素
    返回值：
       返回类别,字符串类型数据
    """
    # TODO
    # 对用户评分user_rating进行类别划分，要求小于等于2.5的数值设定为'good'，大于等于4.5的设定为'best'，其他的设置为'better'
    if ele <= 2.5:
        ele_class ='good'
    elif ele >= 4.5:
        ele_class ='best'
    else:
        ele_class = 'better'

    return ele_class

data_all['user_rating_group'] = data_all['user_rating'].apply(user_rating_group)
print(data_all['user_rating_group'])

'''
对用户评分进行类别划分后，希望依据用户评分类别数据对rating_count_ver、rating_count_tot、user_rating、user_rating_ver进行相关性分析，通过画图，了解各列之间的关系
'''

# TODO
# 根据user_rating_group类别数据进行分组，然后对'rating_count_tot','rating_count_ver','user_rating','user_rating_ver'数据计算组内均值

rating_data = data_all.groupby('user_rating_group')[['rating_count_tot','rating_count_ver', 'user_rating', 'user_rating_ver' ]].mean()

print(rating_data)

# TODO
# 绘制曲线图
# 依据计算出来的rating_data数据，通过绘制2行2列的图，显示user_rating_group对应类别下各数据的走势。
# 由于横轴均是user_rating_group，因此，使用subplots设置参数共享x轴,x轴的刻度标签从左到右依次为'best'、'better'、'good'
# 要求在每个小图的上方设置对应的标题，标题名字为对应的列名。
# fig, ax = plt.subplots(2,2)
# ax[0, 0].plot(rating_data.index, rating_data['rating_count_tot'])
# ax[0, 0].set_title('rating_count_tot')
# ax[0, 1].plot(rating_data.index, rating_data['rating_count_ver'])
# ax[0, 1].set_title('rating_count_ver')
# ax[1, 0].plot(rating_data.index, rating_data['user_rating'])
# ax[1, 0].set_title('user_rating')
# ax[1, 1].plot(rating_data.index, rating_data['user_rating_ver'])
# ax[1, 1].set_title('user_rating_ver')
# plt.tight_layout()



def process(ele):
    """
    功能：
        依据用户评分类别user_rating_group，对'rating_count_ver'数据中的0值替换为组内均值
    参数：
        ele是'rating_count_ver'的一组元素(可以打印出来理解一下)
    返回值：
       对每组内的0值，使用组内均值来计算，且保留两位小数，然后返回修改后的数据。
    """
    # TODO
    # 对每组内的0值，使用组内均值来计算，且保留两位小数，然后返回修改后的数据
    mean = ele.mean()

    ele_ser = ele.replace(0, mean)

    return ele_ser


# rating_count_ver：用户评分计数（当前版本）
data_all['rating_count_data_new'] = data_all.groupby('user_rating_group')['rating_count_ver'].apply(process)
print(data_all.groupby('user_rating_group')['rating_count_ver'].mean())
print(data_all['rating_count_data_new'].mean())

# TODO
# 绘制多变量图
# 根据类别数据user_rating_group，绘制关于'price','size_bytes','lang.num','sup_devices.num'的多变量图(pairplot)，要求对角线上以核密度图显示
#sns.pairplot(data_all, hue='user_rating_group', vars=['price','size_bytes','lang.num','sup_devices.num'], kind='reg', diag_kind='kde')

'''
prime_genre表明了这个app应用所属类型，比如健康类性、旅游类型、咨询类型、游戏类型、音乐类型等等，并且有的app应用会同属于几个类型。这时，我们需要对数据进行处理，以进行下一步分析。
当app应用同属于几个类型时，我们仅保留一个作为这个app应用所属类型
'''

# TODO
# 当app应用所属类型大于2个时，在所属类型prime_genre中，则多个类型之间以&进行类型连接
# 现在我们仅需要保留多个类型中的第一个类型，将该类型作为该app所属类型
# 最后将处理后的数据，以新的列数据保存在原数据集data_all中，列名为prime_genre_class，
# 且将prime_genre列数据从原数据集中删除

data_all['prime_genre_class'] = data_all['prime_genre'].str.split('&').str.get(0)
print(data_all['prime_genre_class'].head())


'''
现在我们只关心用户评分较高的一些app应用的列，需要按照prime_genre_class的数据进行分类，
通过计算每类app应用size_bytes的均值，了解app应用属于哪种类型时，这类app应用占用的内存较高，且支持设备的个数如何。
'''

# 使用透视表对size_bytes数据计算均值，其中行索引设置为prime_genre_class数据，列索引设置为sup_devices.num数据
dataset = data_all[(data_all['user_rating_group']=='best')]


site_bytes_data = dataset.pivot_table(values='size_bytes', index='prime_genre_class', columns='sup_devices.num', aggfunc={'mean'})
print(site_bytes_data)

# TODO
# 绘制热图
# 关于所属类型prime_genre_class和支持设备的数量sup_devices.num对app应用占内存情况进行分析，
# 对数据集site_bytes_data绘制热图(imshow)来直观显示它们之间的关系，
# 要求设置画布大小为15*10，颜色主题为plt.cm.hot_r，需要为图像添加颜色条，以支持设备的数量和所属类型的类别为x、y轴分别设置刻度标签，
# 以及用它们的列名分别设置为x、y轴的标签
plt.figure(figsize=(15, 10))
plt.imshow(site_bytes_data, cmap=plt.cm.hot_r)
plt.colorbar()
plt.xticks(ticks=[x+1 for x in range(site_bytes_data.shape[1])],
           labels=[site_bytes_data.columns[x][1] for x in range(len(site_bytes_data.columns))])
plt.yticks(ticks=[x for x in range(site_bytes_data.shape[0])],
           labels=site_bytes_data.index)



plt.show()