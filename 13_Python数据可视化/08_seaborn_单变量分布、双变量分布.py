import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

'''
单变量分布

核密度估计图 sns.kdeplot(data)

核密度估计(kernel density estimation)是在概率论中用来估计未知的密度函数

通过核密度估计图可以比较直观的看出数据样本本身的分布特征

distplot介绍

sns.distplot(kde=True, hist=True, rug=False)

集合了直方图与核函数估计的功能

只显示直方图  kde=False

只显示核密度估计 hist=False

显示观测条 rug=True

双变量分布

sns.jointplot(x, y, data, kind)

x, y 二维数据，向量或字符串

data，如果 x, y是字符串 data 应该为DataFrame

kind=‘scatter’ 默认，二维散点图

kind=‘hex’，二维直方图

kind=‘kde’，二维核密度估计图
'''

# 核密度估计图
data1 = np.random.normal(size=10000)  # 按正态分布生成数据
sns.kdeplot(data1)


data2 = np.random.random(size=10000)  # 按随机生成数据
sns.kdeplot(data2)


# 直方图
sns.displot(data1, kde=False)
# kde
plt.show()

# 直方图
data1 = np.random.normal(size=100)
sns.distplot(data1, rug=True, color='g')
plt.show()

import pandas as pd

# 双变量分布
df = pd.DataFrame({'x': np.random.randn(500),
                   'y': np.random.randn(500)})

# 二维散点图
sns.jointplot(x='x', y='y', data=df)
# 二维直方图
sns.jointplot(x='x', y='y', data=df, kind='hex')
# 二维核密度估计图
sns.jointplot(x='x', y='y', data=df, kind='kde')
plt.show()