import matplotlib.pyplot as plt
import numpy as np

'''
散点图

plt.scatter(x, y, s, c, marker)

x 横轴的数据 

y 纵轴的数据

s 点的大小，标量或数组

c 点的颜色，标量或数组

marker 点的样式

其中：标量：同时所有的点的大小（颜色）；数组：为每个点设置不同的大小（颜色）

颜色配置： https://matplotlib.org/api/_as_gen/matplotlib.pyplot.colors.html

点的样式： https://matplotlib.org/api/markers_api.html

柱状图

plt.bar(x, height, color)

x 柱子的横坐标

height 柱子的高度（数据）

color 柱子的颜色

注意：绘制多组柱状图时，需要设置不同的横坐标，避免重叠
'''

plt.figure()

# 生成一组数据
x = np.random.randn(50)
y = x * 2

# 可视化结果
plt.scatter(x, y)
plt.show()


plt.figure()

# 生成两组数据
x = np.random.randn(50)
y1 = x ** 2
y2 = x * 2

# 可视化结果
plt.scatter(x, y1)
plt.scatter(x, y2)
plt.show()

# 使用参数
plt.figure()

plt.scatter(x, y1, s=100, c='r', marker='x')
plt.scatter(x, y2, s=20, c='g', marker='s')
plt.show()

# 单组数据
x = [1, 2, 3, 4, 5]
data = [5,2,7,8,2]
plt.bar(x, data)
plt.show()

# 多组数据
x1 = [1, 3, 5, 7, 9]
data1 = [5, 2, 7, 8, 2]
plt.bar(x1, data, color='r')

x2 = np.array(x1) + 0.8  # 需要设置不同的横坐标，避免重叠
data2 = [8, 6, 2, 5, 6]
plt.bar(x2, data2, color='g')

plt.show()


x = np.random.randint(1,30,size=10)
data1 = abs(x-np.mean(x))
data2 = x-np.mean(x)
plt.figure()
plt.scatter(x,data1, s=x*3, c="b", marker="*")
plt.scatter(x,data2,s=x[0]*5,c="r",marker="h")
plt.show()

plt.figure(figsize=(10,5))
year=["2016","2017","2018","2019"]
x_A=[1,3,5,7]
score_A = [40,50,59,60]
w =0.5
plt.bar(x_A,score_A,width=w,label="A")

x_B=np.array(x_A)+w
score_B = [50,70,80,90]
plt.bar(x_B,score_B,width=w,label="B")
plt.xlabel("year")
plt.ylabel("score")
plt.legend()
plt.xticks(x_B-0.25,year)
plt.show()
