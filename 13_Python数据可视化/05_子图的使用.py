import matplotlib.pyplot as plt
import numpy as np

'''
绘制子图

plt.subplots(nrows, ncols, sharex, sharey)

nrows, ncols 分割的行数和列数

sharex, sharey 是否共享x轴、y轴

返回新创建的figure和subplot数组
'''

fig, subplot_arr = plt.subplots(2, 2, figsize=(8, 8))
subplot_arr[0, 0].scatter(np.random.randn(50), np.random.randn(50) * 2)
subplot_arr[0, 1].bar([1,2,3,4,5],[5,2,7,8,2])
subplot_arr[1, 0].hist(np.random.randn(50), bins=10, rwidth=0.8)
subplot_arr[1, 1].imshow(np.random.rand(5, 5))
plt.show()

# 共享x轴
fig, subplot_arr = plt.subplots(2, 2, figsize=(8, 8), sharex=True)

subplot_arr[0, 0].scatter(np.random.randn(50), np.random.randn(50) * 2)
subplot_arr[0, 1].bar([1, 2, 3, 4, 5], [5, 2, 7, 8, 2])
subplot_arr[1, 0].hist(np.random.randn(50), bins=10, rwidth=0.8)
subplot_arr[1, 1].imshow(np.random.rand(5, 5))

plt.show()

# 共享y轴
fig, subplot_arr = plt.subplots(2, 2, figsize=(8, 8), sharey=True)

subplot_arr[0, 0].scatter(np.random.randn(50), np.random.randn(50) * 2)
subplot_arr[0, 1].bar([1, 2, 3, 4, 5], [5, 2, 7, 8, 2])
subplot_arr[1, 0].hist(np.random.randn(50), bins=10, rwidth=0.8)
subplot_arr[1, 1].imshow(np.random.rand(5, 5))

plt.show()


fig,ax =  plt.subplots(2,2,sharex=True)

x = [0,90,180,270,360,450]

data=[np.array(x),np.sin(x),np.cos(x),np.array(x)**2]

for i in range(2):

    for j in range(2):

        if i==0:

            ax[i][j].plot(x,data[i+j])

        else:

            ax[i][j].plot(x,data[i+j+1])

plt.show()