import matplotlib.pyplot as plt
import numpy as np

'''
直方图形式类似于柱状图 ，通过将数据组合在一起来显示分布，比如：显示各年龄段的数据个数

plt.hist(x, bins, rwidth )

x 数据

rwidth 柱子宽度比例，默认为1

bins 分组的个数或分组边界，如4或[20, 30, 40, 50, 60]

除了最后一组，其余组的右边界是开放的
[20, 30), [30, 40), [40, 50), [50, 60]
'''

# 准备数据
population_ages = [22, 55, 62, 45, 21, 22, 34, 42, 42, 4, 89, 82, 80, 80, 75, 65, 54, 44, 43, 42, 48]

# 指定分组个数
bins = 5

plt.hist(population_ages, bins)
plt.show()

# 指定分组边界
bins = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]
plt.hist(population_ages, bins, histtype='bar', rwidth=0.8)
plt.show()