from flask import Flask, request

app = Flask(__name__)

from . import order
from . import user

app.register_blueprint(user.us)
app.register_blueprint(order.ord)