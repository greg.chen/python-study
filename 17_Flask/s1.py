from flask import Flask, request, render_template, jsonify

app = Flask(__name__)

@app.route("/")
def hello_world():
    print(request.path)
    print('name:%s' % request.args.get('name'))
    return "<p>Hello, World!</p>"

@app.route("/hello/world")
def hello():
    print(request.path)
    return render_template('index.html')

@app.route("/rs/json")
def rsJson():
    return jsonify({'k1':'v1'})


if __name__ == '__main__':
    app.run(debug=True)