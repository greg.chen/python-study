from flask import Flask, render_template, request, redirect, session, url_for

app = Flask(__name__)
#app.debug = True
app.secret_key = 'sdfsdfsdfsdf'
#app.config.from_pyfile("settings.py")

app.config.from_object("mysettings.DevelopmentConfig")

USERS = {
    1: {'name': '张三', 'age': 18, 'gender': '男', 'text': "道路千万条"},
    2: {'name': '李四', 'age': 28, 'gender': '男', 'text': "安全第一条"},
    3: {'name': '王五', 'age': 18, 'gender': '女', 'text': "行车不规范"},
}


@app.route('/index', methods=['GET'])
def index():
    user = session.get('user_info')
    if not user:
        url = url_for('l1')
        return redirect(url)

    return render_template('index2.html',user_dict=USERS)

@app.route('/login',methods=['GET','POST'],endpoint='l1')
def login():
    if request.method == "GET":
        return render_template('login.html')
    else:
        user = request.form.get('user')
        pwd = request.form.get('pwd')
        if user == 'cxw' and pwd == '123':
            session['user_info'] = user
            return redirect('/index')


@app.route('/detail/<int:nid>',methods=['GET'])
def detail(nid):
    user = session.get('user_info')
    if not user:
        return redirect('/login')

    info = USERS.get(nid)
    return render_template('detail.html',info=info)


if __name__ == '__main__':
    app.run()
