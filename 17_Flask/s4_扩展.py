from flask import Flask, request, render_template, jsonify,session,redirect

app = Flask(__name__)
#app.debug = True
app.secret_key = 'sdfsdfsdfsdf'
#app.config.from_pyfile("settings.py")

app.config.from_object("mysettings.DevelopmentConfig")


@app.before_request
def before(*args,**kwargs):
    if request.path=='/login':
        return None
    else:
        name=session.get('user_info')
        if not name:
            return redirect('/login')
        else:
            return None
# 2 # 请求走了就会触发,类似于django的process_response,如果有多个，顺序是从下往上执行
@app.after_request
def after(response):
    print('我走了')
    return response


# 3 before_first_request 项目启动起来第一次会走，以后都不会走了，也可以配多个（项目启动初始化的一些操作）
@app.before_first_request
def first():
    print('我的第一次')


# 4 每次视图函数执行完了都会走它，# 用来记录出错日志
@app.teardown_request  # 用来记录出错日志
def ter(e):
    print(e)
    print('我是teardown_request ')


# 5 errorhandler绑定错误的状态码，只要码匹配，就走它
@app.errorhandler(404)
def error_404(arg):
    return render_template('error.html', message='404错误')


USERS = {
    1: {'name': '张三', 'age': 18, 'gender': '男', 'text': "道路千万条"},
    2: {'name': '李四', 'age': 28, 'gender': '男', 'text': "安全第一条"},
    3: {'name': '王五', 'age': 18, 'gender': '女', 'text': "行车不规范"},
}


@app.route('/index', methods=['GET'])
def index():
    user = session.get('user_info')
    if not user:
        url = url_for('l1')
        return redirect(url)

    return render_template('index2.html',user_dict=USERS)

@app.route('/login',methods=['GET','POST'],endpoint='l1')
def login():
    if request.method == "GET":
        return render_template('login.html')
    else:
        user = request.form.get('user')
        pwd = request.form.get('pwd')
        print(user)
        print(pwd)
        if user == 'cxw' and pwd == '123':
            session['user_info'] = user
            return redirect('/index')


@app.route('/detail/<int:nid>',methods=['GET'])
def detail(nid):
    user = session.get('user_info')
    if not user:
        return redirect('/login')

    info = USERS.get(nid)
    return render_template('detail.html',info=info)


if __name__ == '__main__':
    app.run()
