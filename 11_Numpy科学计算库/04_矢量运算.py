import numpy as np


arr1=np.array([[0,1,2],[2,3,3]])
arr2=np.array([1,2,4])
arr=arr1*arr2
print(arr)

arr=arr.reshape(3,2)
print(arr)
print(arr.shape)


num_arr = np.array([1,3,5,6,8])
for i in range(len(num_arr)):
    num_arr[i] = num_arr[i] * 2

print(num_arr)

a = np.arange(100, dtype=float)
b = np.arange(100, 0, -1, dtype=float)
print(a)
print(b)

print(np.arange(3) + 5)

arr = np.random.randn(2,3)
print(arr)
# 1. ceil：向上取最接近的整数
print("np.ceil",np.ceil(arr))

# 2. floor：向下最接近的整数
print("floor",np.floor(arr))

# 3. rint：四舍五入
print("rint",np.rint(arr))

# 4. Isnan：判断元素是否为NaN
print("isnan",np.isnan(arr))
# 5. multiply：元素相乘
#[0 1 2] * 5
print("np.multiply",np.multiply(np.arange(3), 5))
print("np.arange(3) * 5",np.arange(3) * 5)
# 6. divide：元素相除
print("np.divide",np.divide(np.arange(3), 5))
# 7. dot：矩阵相乘

arr1=[[1,3],[2,4]]
arr2=[[2,1],[4,1]]

print(np.multiply(arr1,arr2))