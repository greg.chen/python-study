import numpy as np
# 常用的统计方法：

data = np.arange(10).reshape(5, 2)
print(data)
# np.mean：求均值
print("mean",np.mean(data))
print("np.mean(data.txt, axis=0)",np.mean(data, axis=0))
print("np.mean(data.txt, axis=1)",np.mean(data, axis=1))

# np.sum：求和
print("sum",np.sum(data))
print("np.sum(data.txt, axis=0)",np.sum(data, axis=0))
print("np.sum(data.txt, axis=1)",np.sum(data, axis=1))

# np.max：求最大值
print("max",np.max(data))
print("np.max(data.txt, axis=0)",np.max(data, axis=0))
print("np.max(data.txt, axis=1)",np.max(data, axis=1))

# np.min：求最小值
#
# np.std：求标准差
print("std",np.std(data))

# np.var：求方差
print("var",np.var(data))

# 常用方法：
#
# 是否所有元素满足条件：np.all()
print("all",np.all(data > 5))

# 是否至少一个元素满足条件：np.any()
print("any",np.any(data > 5))
# 求唯一值并返回排序结果：np.unique()
arr = np.array([[1, 2, 1], [2, 3, 4]])
print(arr)
print("unique",np.unique(arr))

# 最大值索引
print("argmax",np.argmax(data))
print("np.argmax(data.txt, axis=0)",np.argmax(data, axis=0))
print("np.argmax(data.txt, axis=1)",np.argmax(data, axis=1))

arr = np.array([[1,2,3],[4,5,6],[7,8,9]])

print(np.argmin(arr[1:],axis=0))