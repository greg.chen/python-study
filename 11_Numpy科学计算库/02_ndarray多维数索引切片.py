
#NumPy和SciPy
import numpy as np

l = [1, 2, 3, 4, 5, 6]
data = np.array(l)
print(data)
print(l[0],l[-1],l[3:])

# 多维数组索引
# [
#   [0 1 2]
#   [3 4 5]
#   [6 7 8]
# ]

print(np.arange(9))
data = np.arange(9).reshape(3,3)
print(data)
print(data[2])
print("data.txt[:2, 1:]",data[:2, 1:])
print("data.txt[2, :]",data[2, :])
print("data.txt[1, :2]",data[1, :2])

# 条件索引
# 找出数据中大于4的数据
is_gt = data > 4
print("is_gt",is_gt)
print("data.txt[is_gt]",data[is_gt])

# 找出数据中大于4的偶数
print("data.txt[ (data.txt > 4) & (data.txt % 2 == 0)]",data[ (data > 4) & (data % 2 == 0)])

data = np.array([1,2,3,4,5,6,7,8,9]).reshape(3, 3)
print(data)
#转置
print("data.txt.T",data.T)
print("transpose",np.transpose(data))
print("swapaxes",np.swapaxes(data, 0, 1))


data = np.arange(9).reshape(3,3)
print(data)
print("----",data[data[:, 1]>4])

l = [['767' ,1],
 ['1080' ,1],
 ['1077', 0],
]
data = np.array(l)
print(data[data[:, 1]>0])