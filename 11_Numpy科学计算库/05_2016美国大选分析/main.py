import utils

filename = 'presidential_polls.csv'


def main():
    use_cols = ['enddate', 'rawpoll_clinton', 'rawpoll_trump']
    data_array = utils.load_data(filename, use_cols)

    data_array = utils.process_date(data_array)

    utils.get_month_stats(data_array)

if __name__ == "__main__":
    main()
