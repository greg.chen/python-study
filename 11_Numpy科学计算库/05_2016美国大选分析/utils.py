import numpy as np
import datetime


def load_data(filename, use_cols):

    with open(filename, mode='rt') as f:
        col_name_str = f.readline()[:-1]

    # 将字符拆分, 并组成列表
    col_name_lst = col_name_str.split(',')

    #获取相应列名的索引号
    use_col_index_lst = [ col_name_lst.index(use_col_name) for use_col_name in use_cols]

    #读取数据
    data_array = np.loadtxt(filename, delimiter=",", skiprows=1, dtype=str, usecols=use_col_index_lst)

    return data_array;


def process_date(data_array):
    endate_list = data_array[:,0].tolist();

    # 统一yy/dd/mm
    endate_list = [enddate.replace('-','/') for enddate in endate_list]
    print(endate_list[:10])
    # 字符串转成日期
    date_list = [datetime.datetime.strptime(enddate, '%m/%d/%Y') for enddate in endate_list]
    month_list = [ '{}-{:02d}'.format(date_obj.year, date_obj.month)  for date_obj in date_list]
    month_array = np.array(month_list)
    data_array[:,0]=month_array
    return data_array

def get_month_stats(data_array):

    months = np.unique(data_array[:,0])
    print(months)
    for month in months:
        filtered_data = data_array[data_array[:,0] == month]

        try:
            filtered_poll_data = filtered_data[:, 1:].astype(float)
        except ValueError:
            continue

        result = np.sum(filtered_poll_data, axis=0)
        print(("{}, Clinton票数: {}, Trump票数: {}").format(month, result[0], result[1]))