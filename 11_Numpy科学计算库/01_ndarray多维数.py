import numpy as np

l = [1, 2, 3, 4, 5, 6]
data = np.array(l)
print(data)

print('维度个数', data.ndim)
print('各维度大小: ', data.shape)
print('数据类型: ', data.dtype)

l = [[1., 2., 3.], [4., 5., 6.]]
data = np.array(l)
print(data)
print('维度个数', data.ndim)
print('各维度大小: ', data.shape)
print('数据类型: ', data.dtype)


# 三维数组
l = [
        [
            [1, 2],
            [3, 4]
        ],
        [
            [5, 6],
            [7, 8]
        ]
]
data = np.array(l)
print("三维数组",data)

print('维度个数', data.ndim)
print('各维度大小: ', data.shape)
print('数据类型: ', data.dtype)

zeros_arr = np.zeros((3,4))
print(zeros_arr)
# np.ones()
ones_arr = np.ones((2, 3))
print(ones_arr)

rand_arr = np.random.rand(4,5)
print(rand_arr)

arr = np.arange(10)
print(arr)
# 转换
arr2 = arr.reshape((5, 2))
print("reshape",arr2)

arr3 = arr2.astype(float)
print(arr3.dtype)
print(arr3)

print(np.arange(5))
print(np.array([0,1,2,3,4]))
print(np.ndarray([0,1,2,3,4]))