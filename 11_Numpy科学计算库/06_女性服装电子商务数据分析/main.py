import numpy as np
import csv

def main():
    dataset = get_alldata('womens_clothing_e-commerce_reviews.csv')
    id_count_lst = get_id_count_arr(dataset)
    print("id_count_lst",id_count_lst)

    id_recom_ratio_lst = cal_recom_num(dataset,['767','1078'])
    print(id_recom_ratio_lst)
    id_pos_sum_lst, id_name_lst = cal_pos_num(dataset,['767','1078'])
    print(id_pos_sum_lst)
    print(id_name_lst)

def get_alldata(filename):
    """
    功能：
        通过文件的路径，得到所有数据，然后根据给定的列名，得到索引，再根据索引获得对应的数据集 ，数据集的每一行都是一个客户的评论
    参数：
        文件的路径：womens_clothing_e-commerce_reviews.csv
    返回值：
        返回给定列的所有数据，以array的形式返回

    """
    with open(filename, 'r') as csv_file:
        col_name_all = csv_file.readline()[:-1]
        colname_str = "Clothing ID,Recommended IND,Positive Feedback Count,Class Name"

        col_name_all = col_name_all.split(",")
        colname_lst = colname_str.split(",")

        # TODO
        # colindex_lst是列表，依据colname_lst提供的字段，获得该字段在col_name_all中的索引号
        # None是需要补充代码的地方
        colindex_lst = [col_name_all.index(col_name) for col_name in colname_lst]
        print(colindex_lst)

        # TODO
        # dataset的每列的列名依次为"服装号(Clothing ID),推荐标识(Recommended IND),积极反馈（Positive Feedback Count）,服装类型（Class Name）"，
        # 因为数据文件中的数据格式的原因，此处不建议使用np.loadtxt读出数据，建议使用csv模块的reader函数读取csv_file的数据，依据得到的索引号colindex_lst，获得对应列的数据集到dataset列表中。
        # 最后要求：dataset中的每个元素是列表，比如，['767', '1', '0', 'Intimates']
        dataset = []
        reader = csv.reader(csv_file)
        for row in reader:
            data = [row[index] for index in colindex_lst]
            dataset.append(data)

        return np.array(dataset)


def get_id_count_arr(dataset):
    """
    功能：
        由于数据集的每一行都是一个客户的评论，因此这个函数根据Clothing ID来分析对应的评论数，
    参数：
        "Clothing ID,Recommended IND,Positive Feedback Count,Class Name"列的所有数据集，数据集的每一行都是一个客户的评论
    返回值：
        返回评论大于400的所有Clothing ID号
    """
    # TODO
    # 计算每个Clothing ID的评论数，将Clothing ID的数量大于400的数据放入id_count_lst中
    id_count_lst = []
    id_all_list = np.unique(dataset[:, 0])
    for id in id_all_list:
        count = dataset[dataset[:, 0] == id].size
        if count > 400:
            id_count_lst.append(id)

    return id_count_lst


def cal_recom_num(dataset, id_lst):
    """
    功能：
        计算每个Clothing ID代表的服装被评论的次数列表，以及被推荐的次数列表
    参数：
        dataset：需要被分析的数据集
        id_lst：需要被分析的唯一Clothing ID号
    返回值：
        id_recom_ratio_lst：返回推荐次数占评论次数的比例，即得到这个Clothing ID代表的服装的受欢迎程度
    """
    id_recom_ratio_lst =[]
    for id in id_lst:
        comment_count = dataset[dataset[:,0] == id].size
        dataset_recom = dataset[dataset[:,1] == '1']
        recom_count = dataset_recom[dataset_recom[:,0] == id].size
        print("comment_count", comment_count)
        print("recom_count", recom_count)
        id_recom_ratio_lst.append([id, recom_count/comment_count])


    # TODO
    # dataset的每列的列名依次为"服装号(Clothing ID),推荐标识(Recommended IND),积极反馈（Positive Feedback Count）,服装类型（Class Name）"，
    # 故被推荐次数的索引为1，该列中的数据只有0和1，1表示推荐，0表示不推荐
    # 现在需要通过id_lst和dataset，利用数组的性质，计算每个Clothing ID号代表服装的被推荐的比例，即推荐次数比上评论次数

    return id_recom_ratio_lst


def cal_pos_num(dataset, id_lst):
    """
    功能：
        Positive Feedback Count列是每个评论被赞同的次数，计算每个Clothing ID的评论被赞同的总次数。
    参数：
        dataset：需要被分析的数据集
        id_lst：需要被分析的唯一Clothing ID号

    返回值：
        id_pos_sum_lst中的元素为每个Clothing ID的评论被赞同的和（总次数）。
        id_name_lst中的每个元素是Clothing ID的服装类型。
    """
    # 每个Clothing ID进行正反馈次数加和的列表
    id_pos_sum_lst = []
    # 每个Clothing ID的类型名称
    id_name_lst = []

    # TODO
    # dataset的每列的列名依次为"服装号(Clothing ID),推荐标识(Recommended IND),积极反馈（Positive Feedback Count）,服装类型（Class Name）"，
    # 对每个Clothing ID被评论的正反馈次数进行加和统计和对应的服装类型Class Name
    for id in id_lst:
        dataset_id = dataset[dataset[:,0] == id]
        id_name_lst.append(dataset_id[0][3])
        id_pos_sum = np.sum(dataset_id[:,2].astype(int), axis=0)
        id_pos_sum_lst.append(id_pos_sum)

    return id_pos_sum_lst, id_name_lst

if __name__ == "__main__":
    main()
