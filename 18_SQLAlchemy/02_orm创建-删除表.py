# 创建一个个类（继承谁？字段怎么写）
import datetime
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
# 字段和字段属性
from sqlalchemy import Column, Integer, String, Text, ForeignKey, DateTime, UniqueConstraint, Index

# 制造了一个类，作为所有模型类的基类
Base = declarative_base()

class User(Base):
    __tablename__ = 'users'  # 数据库表名称(固定写法)，如果不写，默认以类名小写作为表的名字
    id = Column(Integer, primary_key=True)  # id 主键
    # mysql中主键自动建索引：聚簇索引
    # 其他建建的索引叫：辅助索引
    name = Column(String(32), index=True, nullable=False)  # name列，索引，不可为空
    email = Column(String(32), unique=True)  # 唯一
    # #datetime.datetime.now不能加括号，加了括号，以后永远是当前时间
    ctime = Column(DateTime, default=datetime.datetime.now) # default默认值
    extra = Column(Text, nullable=True)

    #类似于djagno的 Meta
    __table_args__ = (
        UniqueConstraint('id', 'name', name='uix_id_name'), #联合唯一
        Index('ix_id_name', 'name', 'email'), #索引
    )



# 创建表
def create_table():
    # 创建engine对象
    engine = create_engine(
        "mysql+pymysql://root:root@127.0.0.1:3306/testing?charset=utf8",
        max_overflow=0,  # 超过连接池大小外最多创建的连接
        pool_size=5,  # 连接池大小
        pool_timeout=30,  # 池中没有线程最多等待的时间，否则报错
        pool_recycle=-1  # 多久之后对线程池中的线程进行一次连接的回收（重置）
    )
    # 通过engine对象创建表
    Base.metadata.create_all(engine)

# 删除表
def drop_table():
    # 创建engine对象
    engine = create_engine(
        "mysql+pymysql://root:root@127.0.0.1:3306/testing?charset=utf8",
        max_overflow=0,  # 超过连接池大小外最多创建的连接
        pool_size=5,  # 连接池大小
        pool_timeout=30,  # 池中没有线程最多等待的时间，否则报错
        pool_recycle=-1  # 多久之后对线程池中的线程进行一次连接的回收（重置）
    )
    # 通过engine对象删除所有表
    Base.metadata.drop_all(engine)

if __name__ == '__main__':
    create_table()
    #drop_table()