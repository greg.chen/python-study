from multiprocessing import Queue


q = Queue(5)
# 当队列放满,程序会阻塞
# 先进先出
q.put(111)
q.put(222)
print("full",q.full()) # 判断当前队列是否满

# 当队列为空时,程序会阻塞
v1 = q.get()
v2 = q.get()
#v3 = q.get_nowait()
# v3 = q.get(timeout=3)
print(v1, v2)

