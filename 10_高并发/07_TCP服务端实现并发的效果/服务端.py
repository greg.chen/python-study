import socket
from threading import Thread
from multiprocessing import Process

server = socket.socket() #不加参数, 默认TCP
server.bind(('127.0.0.1',8080))
server.listen(5)


def talk(conn):
    #通信循环
    while True:
        try:
            data = conn.recv(1024)
            if len(data) == 0:
                break
            print(data.decode('utf-8'))
            conn.send(data)

        except ConnectionResetError as e:
            print(e)
    conn.close()

while True:
    conn, addr = server.accept()
    t = Thread(target=talk, args=(conn,))
    t.start()

