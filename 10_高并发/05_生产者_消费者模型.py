from multiprocessing import Queue, Process, JoinableQueue
import time
import random

def producer(name,food,q):
    for i in range(10):
        data="%s生产了%s%s"%(name,food, i)
        time.sleep(random.randint(1,3))
        print(data)
        q.put(data)

def consumer(name,q):
    while True:
        food = q.get()
        if food is None:
            break
        time.sleep(random.randint(1,3))
        print('%s吃了%s'%(name, food))
        q.task_done()

if __name__ == '__main__':
    q = JoinableQueue()
    p1 = Process(target=producer, args=('JJ','包子',q,))
    p2 = Process(target=producer, args=('GG', '馒头', q,))

    c1 = Process(target=consumer, args=('DD', q,))
    c2 = Process(target=consumer, args=('CC', q,))

    p1.start()
    p2.start()

    #将消费者设置成守护进程
    c1.daemon = True
    c2.daemon = True
    c1.start()
    c2.start()


    p1.join()
    p2.join()

    q.join() # 等待队列中所有的数据被取完

    # q.put(None)
    # q.put(None)


