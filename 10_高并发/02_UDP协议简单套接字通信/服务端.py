import socket

# 数据报协议  UDP议
server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server.bind(('127.0.0.1', 8066))

while True:
    data, client_addr = server.recvfrom(1024)
    server.sendto(data.upper(), client_addr)


server.close()