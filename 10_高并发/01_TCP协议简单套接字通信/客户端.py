from socket import *
import struct
import json

client = socket(AF_INET, SOCK_STREAM)

client.connect(("127.0.0.1", 8066))

while True:
    msg = input("输入要发送的消息>>>:")
    if len(msg) == 0:
        continue
    client.send(msg.encode('utf-8'))

    # 先收固定长度的头, 解析数据描述信息: 数据总大小
    x = client.recv(4)
    header_len = struct.unpack('i',x)[0]
    # 接收头
    json_str_bytes = client.recv(header_len)
    json_str = json_str_bytes.decode('utf-8')
    header_dic = json.loads(json_str)
    print(header_dic)

    # 基于接收头的数据总大小, 接收真实的数据
    total_size = header_dic['total_size']
    print(total_size)
    recv_size = 0
    cmd_res=b''
    while recv_size < total_size:
        recv_data = client.recv(1024)

        recv_size +=len(recv_data)
        cmd_res +=recv_data
    else:
        print(cmd_res.decode('utf-8'))

