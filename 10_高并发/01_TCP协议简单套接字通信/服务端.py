import json
from socket import *
import struct
# 流式协议 TCP协议
server = socket(AF_INET, SOCK_STREAM)
server.bind(('127.0.0.1', 8066))
#半连接池的大小
server.listen(5)

while True:
    conn,client_addr = server.accept()
    print(conn)
    print("client_addr", client_addr)

    while True:
        #最大接收数据量1024 bytes
        data = conn.recv(1024)
        if len(data)==0:
            break
        #发消息
        res = '''
        因为python跟java在数据类型转换方面语法不同，所以总是搞混，特此记录下来，方便查阅：

        在python中：
        
        字符串str转换成int: int_value = int(str_value)
        
        int转换成字符串str: str_value = str(int_value)
        
        int -> unicode: unicode(int_value)
        
        unicode -> int: int(unicode_value)
        
        str -> unicode: unicode(str_value)
        
        unicode -> str: str(unicode_value)
        
        int -> str: str(int_value)
        
        str -> int: int(str_value)
        ————————————————
        版权声明：本文为CSDN博主「shanliangliuxing」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
        原文链接：https://blog.csdn.net/shanliangliuxing/article/details/7920400'''

        total_size = len(res)

        print(len(res))
        # 先发头信息
        # int 固定长度
        header_dic ={
            "filename":"a.txt",
            "total_size": total_size,
            "md5":"1234134"
        }

        json_str = json.dumps(header_dic)
        json_str_bytes = json_str.encode('utf-8')

        header_size = struct.pack('i', len(json_str_bytes))

        # 先发头信息大小
        conn.send(header_size)
        # 发头信息
        conn.send(json_str_bytes)
        # 传输数据
        conn.send(res.encode('utf-8'))

    #关闭连接
    conn.close()

phone.close()