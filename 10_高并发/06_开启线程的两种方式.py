import os
import time
from threading import Thread, current_thread,active_count

def task(name):
    print('%s is runing'%name, os.getpid(), current_thread().name)
    time.sleep(5)
    print('%s is over', current_thread().name)


if __name__ == '__main__':
    t = Thread(target=task, args=('egon',))
    t.daemon=True # 主线程运行结束后, 守护线程结束
    t.start()

    # 主线程运行结束后, 会等待非守护线程结束
    print('主', os.getpid(), current_thread().name, active_count())