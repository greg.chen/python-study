from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
import time


pool = ThreadPoolExecutor(5)

def task(n):
    print(n)
    time.sleep(1)
    return n**2

pool.submit(task, 1)

t_list = []
for i in range(20):
    res = pool.submit(task, i)
    t_list.append(res)

pool.shutdown()
for t in t_list:
    print('>>>>', t.result())