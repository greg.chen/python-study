import socket

from gevent import monkey
monkey.patch_all()
from gevent import spawn




def talk(conn):
    #通信循环
    while True:
        try:
            data = conn.recv(1024)
            if len(data) == 0:
                break
            print(data.decode('utf-8'))
            conn.send(data)

        except ConnectionResetError as e:
            print(e)
    conn.close()

def server(ip,port):
    server = socket.socket()  # 不加参数, 默认TCP
    server.bind((ip, port))
    server.listen(5)

    while True:
        conn, addr = server.accept()
        t = spawn(talk, conn)
        t.start()


if __name__ == '__main__':
    g1 = spawn(server, '127.0.0.1',8080)
    g1.join()