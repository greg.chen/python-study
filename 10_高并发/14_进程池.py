from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor
import time


def task(n):
    print(n)
    time.sleep(1)
    return n**2

def call_back(n):
    print("call_back",n.result())

if __name__ == '__main__':
    # 默认os.cpu_count()
    pool = ProcessPoolExecutor()

    t_list = []
    for i in range(20):
        res = pool.submit(task, i).add_done_callback(call_back)
        t_list.append(res)

    #pool.shutdown()
    # for t in t_list:
    #     print('>>>>', t.result())