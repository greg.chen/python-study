from multiprocessing import Process
import time

def task(name):
    print('%s is runing' % name)
    time.sleep(3)
    print('%s is over'% name)


class MyProcess(Process):
    def run(self):
        print('hello' )
        time.sleep(1)
        print('world')

if __name__ == '__main__':
    p = Process(target=task, args=('Greg',))
    p.start()
    p.join() # 主进程等待子进程运行结束
    print('主')

    p = MyProcess();
    p.start();
    print('主')