import socketserver


class MyRequestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        print(self.request)
        print(self.client_address)
        while True:
            msg = self.request.recv(1024)
            print(msg)
            if len(msg) == 0:
                break
            self.request.send(msg.upper())

        self.request.close()


server = socketserver.ThreadingTCPServer(('127.0.0.1', 8066), MyRequestHandler)
server.serve_forever()
