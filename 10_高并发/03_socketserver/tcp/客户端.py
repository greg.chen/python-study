from socket import *
import struct
import json

client = socket(AF_INET, SOCK_STREAM)

client.connect(("127.0.0.1", 8066))

while True:
    msg = input("输入要发送的消息>>>:")
    if len(msg) == 0:
        continue
    client.send(msg.encode('utf-8'))

    # 先收固定长度的头, 解析数据描述信息: 数据总大小
    res = client.recv(1024)
    print(res.decode('utf-8'))


