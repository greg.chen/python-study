from multiprocessing import Queue, Process


def producer(q):
    q.put('老板好')


if __name__ == '__main__':
    q = Queue()
    p = Process(target=producer, args=(q,))
    p.start()
    v = q.get()
    print(v)