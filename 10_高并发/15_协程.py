'''
进程:资源单位
线程:执行单位
协程:这个概念完全是程序员自己意淫出来的 根本不存在
		单线程下实现并发
		我们程序员自己再代码层面上检测我们所有的IO操作
		一旦遇到IO了 我们在代码级别完成切换
		这样给CPU的感觉是你这个程序一直在运行 没有IO
		从而提升程序的运行效率

多道技术
	切换+保存状态
	CPU两种切换
		1.程序遇到IO
		2.程序长时间占用

TCP服务端
	accept
	recv

代码如何做到
	切换+保存状态

切换
	切换不一定是提升效率 也有可能是降低效率
	IO切			提升
	没有IO切 降低

保存状态
	保存上一次我执行的状态 下一次来接着上一次的操作继续往后执行
	yield
'''

# gevent 模块
import time
from gevent import monkey
# 猴子补丁, 在使用gevent模块必须导入
monkey.patch_all()

from gevent import spawn, monkey


def func1():
    print("func1")
    time.sleep(2)
    print("func1")

def func2():
    print("func2")
    time.sleep(3)
    print("func2")

start_time = time.time()
f1 = spawn(func1)
f2 = spawn(func2)
f1.join()
f2.join() # 等待被检测的任务执行完毕 再往后继续执行
print(time.time() - start_time) #5.0073957443237305   #3.005511999130249










