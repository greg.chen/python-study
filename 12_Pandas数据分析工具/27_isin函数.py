import pandas as pd
import numpy as np

'''
isin()
判断数据是否存在，返回布尔型向量
可用于数据的过滤

Series.isin(vals): 判断Series的内容是否在vals中
Index.isin(vals): 判断索引值是否在vals中
DataFrame.isin(vals)
vals为列表时，判断DataFrame中的数据是否在vals中
vals为字典时，判断DataFrame中对应列的数据是否在vals中

'''

s = pd.Series(np.arange(5),
              index=['a', 'b', 'c', 'd', 'e'])

# 返回值为布尔值向量
print('返回值为布尔值向量',s.isin([2, 4, 6]))

# 数据过滤
print("数据过滤",s[s.isin([2,4,6])])
# Index.isin()
print("Index.isin()", s.index.isin(['c','d']))
print("Index.isin()", s[s.index.isin(['c','d'])])

df = pd.DataFrame({'col1': [1, 2, 3, 4],
                   'col2': ['a', 'b', 'f', 'n'],
                   'col3': ['a', 'n', 'c', 'n']})
print(df)
vals1 = ['a', 'b', 1, 3]
#  所有值过滤
print('所有值过滤', df[df.isin(vals1)])

# 按列过滤
vals2 = {'col1': [1, 3],
         'col2': ['a', 'b']}
print('按列过滤', df[df.isin(vals2)])

df = pd.DataFrame({'countries': ['US', 'UK', 'Germany', 'China']})
c = ['US', 'UK']
print(df[~df['countries'].isin(c)])