import pandas as pd
import numpy as np

'''
基本索引的功能强大，通常需要包含多种功能，如：
    单个 label
    切片操作
    遮罩索引
…
但是，如果数据量过大时会影响数据的处理速度

如果只需要访问标量（一个值）数据，没必要使用loc或iloc
at[ ] 和 iat[ ] 可以快速对标量数据进行获取/赋值

'''

raw_df1 = pd.read_csv('2016_happiness.csv')
raw_df2 = raw_df1.copy()

#loc与at数据获取对比
print("loc与at数据获取对比", raw_df1.loc[50, 'Country'])
print("loc与at数据获取对比", raw_df1.at[50, 'Country'])

#iloc与iat对比
print("loc与at数据获取对比", raw_df1.iloc[0, 0])
print("loc与at数据获取对比", raw_df1.iat[0, 0])

