import pandas as pd
import numpy as np

'''
层级索引：层级索引的对象是MultiIndex对象

设置多个索引列：

set_index([‘a’,’b’],inplace=True)，其中a的列是第一级行索引，在最外层，b的列设置为第二级行索引，在次外层的位置，a和b的先后顺序是有意义的。

选取子集：

1. 外层选取loc[‘outer_index’]，outer_index指外层索引中指定索引行的名称，比如行索引country为[“A”,”B”,”C”,”D”]，则loc[“B”]表示获取索引行为B的分组数据

2. 内层选取loc[“outer_index”,”inner_index”]，表示从外层索引为outer_index的分组中选取内层索引行为inner_index的分组数据。
'''

data = pd.read_csv('2016_happiness.csv', usecols=['Country', 'Region', 'Happiness Rank', 'Happiness Score'])

# 数据预览
print(data.head())

data.set_index(['Region', 'Country'], inplace=True)
print(data)

# 外层选取
print(data.loc['Western Europe'])

# 内层选取
print(data.loc['Australia and New Zealand', 'New Zealand'])

print("函数索引",data.loc[lambda  df:df['Happiness Score'] > 7.5])
print("列过滤",data[data['Happiness Score'] > 7.5])