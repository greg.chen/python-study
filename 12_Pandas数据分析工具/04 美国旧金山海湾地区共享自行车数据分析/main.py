# TODO
# 导入 pandas 和 numpy 函数库
import pandas as pd
import numpy as np




if __name__ == '__main__':
    # TODO
    # 导入csv文件
    stations = pd.read_csv('./data/stations.csv')
    trips1 = pd.read_csv('./data/trips1.csv')
    trips2 = pd.read_csv('./data/trips2.csv')
    trips3 = pd.read_csv('./data/trips3.csv')

    print(stations.head())
    print(trips1.head())
    print(trips2.head())
    print(trips3.head())

    #### 2. 将 trips1、trips2、trips3 合并为一个Dataframe, 命名为 trips
    trips = pd.concat([trips1, trips2, trips3])
    print("trips--->",trips.head())

    #### 3. 将 staions 中所有列名称前添加字段'start_'，并且将 start_id 设置为列索引
    stations.columns = map(lambda c:'start_'+c, stations.columns)

    # 将 start_id 设置为列索引，使用set_index函数
    stations.set_index('start_id', inplace=True)
    print(stations.head())

    #### 4. 将 trips 和 stations 按照起始车站id进行字段匹配并合并，保留所有匹配成功的信息
    trips_stations = pd.merge(trips, stations,how='inner', on='start_id')
    print(trips_stations.head())

    #### 5. 将trips_stations 导出为' trips_stations.csv'文件
    trips_stations.to_csv("trips_stations.csv")

    #### 6. 查看 trips_stations 中是否包含有重复值，并且将重复值删除
    print(trips_stations.duplicated().all())
    print(trips_stations.drop_duplicates(inplace=True))
    print(trips_stations.duplicated().all())

    #### 7. 查看 trips_stations 中是否包含有缺失值，并且处理缺失值
    isNA_trips_stations = trips_stations.isnull()
    print(trips_stations[isNA_trips_stations.any(axis=1)])

    # 修改原数据,使用中位数填补'start_docks'列的缺失值，使用fillna函数
    trips_stations['start_docks'] = trips_stations['start_docks'].fillna(trips_stations['start_docks'].mean())
    # 将 lat、long 中的缺失值设置为‘未知’
    trips_stations['start_lat'] = trips_stations['start_lat'].fillna('未知')
    trips_stations['start_long'] = trips_stations['start_long'].fillna('未知')

    # 删除缺失值
    trips_stations.dropna(inplace=True)

    isNA_trips_stations = trips_stations.isnull()
    print(trips_stations[isNA_trips_stations.any(axis=1)])

    #### 8. 去除 trips_stations 中 ‘start_name’ 列中每个字符串左右两边的空格和’#‘
    trips_stations['start_name'] = trips_stations['start_name'].str.strip().str.strip('#')

    #### 9. 将 'start_date' 和 'end_date' 中日期和时间进行拆分, 并分别记录在 'start_date'、'start_time'、'end_date'、'end_time'列中
    trips_stations_start_date = trips_stations['start_date'].str.split(' ', expand=True)
    trips_stations['start_date'] = trips_stations_start_date.get(0)
    trips_stations['start_time'] = trips_stations_start_date.get(1)

    trips_stations_end_date = trips_stations['end_date'].str.split(' ', expand=True)
    trips_stations['end_date'] = trips_stations_end_date.get(0)
    trips_stations['end_time'] = trips_stations_end_date.get(1)



    #### 10.  将每个终点车站数进行分组，分为 <br>
    ####  '13以下', '13到15', '15到17', '17到19', '19到21', '21到23', '23到25', '25以上' 几类，<br>
    #### 并且在'start_docks'列右侧增加一列'start_docks_classification'记录每个车站数所属的分类
    print(trips_stations['start_docks'])
    #df['Level'] = pd.cut(df['Score'], bins=3, labels=['C', 'B', 'A'])
    bins = [-np.inf, 13, 15, 17,19,21,23,25, np.inf]
    st_lablels = ['13以下', '13到15', '15到17', '17到19','19到21', '21到23', '23到25', '25以上']
    trips_stations['start_docks_classification'] = pd.cut(trips_stations['start_docks'], bins=bins, labels=st_lablels)
    print(trips_stations['start_docks_classification'].head())

    #### 11. 将'subscription_type'转化为虚拟变量，添加在dateframe的最后一列
    trips_stations_dummies = pd.get_dummies(
        trips_stations,
        columns=['subscription_type'],
        prefix=['subscription_type'],
        prefix_sep="_",
        dummy_na=False,
        drop_first=False
    )

    trips_stations_dummies['subscription_type'] = trips_stations['subscription_type']
    print(trips_stations_dummies.head())

    trips_stations_dummies.to_html()
