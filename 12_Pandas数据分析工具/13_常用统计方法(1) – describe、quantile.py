import pandas as pd
import numpy as np

data = pd.read_csv("2016_happiness.csv")
print(data.describe())

print("data.quantile(q=0.5)",data.quantile(q=0.5))
print("data.quantile(q=0.25)",data.quantile(q=0.25))

df_obj = pd.DataFrame(np.arange(15).reshape(5,3),columns=['A','B','C'])
print(df_obj)