import pandas as pd
import numpy as np

ser_obj = pd.Series(np.random.randn(1000),
                    index=pd.date_range('20190101', periods=1000))
ser_obj = ser_obj.cumsum()
print(ser_obj.head())

rolling_obj = ser_obj.rolling(window=3)
print("rolling_obj.mean().head()",rolling_obj.mean().head())

# 验证：
# 0-3数据的均值
print("ser_obj[0:3]",ser_obj[0:3],ser_obj[0:3].mean())

# 1-4数据的均值
print(ser_obj[1:4].mean())

import matplotlib.pyplot as plt
plt.figure(figsize=(15, 5))

ser_obj = pd.Series(range(10))
print(ser_obj.rolling(window=3).sum()[5])
print(ser_obj.rolling(window=3, center=True).mean()[5])
