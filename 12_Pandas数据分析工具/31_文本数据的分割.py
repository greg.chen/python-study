import pandas as pd
import numpy as np

'''
文本数据分割str.split(pat, n, expand)

将字符串向量中的每个字符串分割
参数：
pat: 分割基于的字符
n: 分割的次数，None, 0, -1表示全部分割
expand: 是否将分割的子串扩展为列，
默认为False，表示不扩展，返回的结果是Series类型
设为True，每个分割的结果为一列，返回的结果是DataFrame类型
获取分割的结果，str.get()或str[ ]
只能在expand=False时使用

'''

email = 'zhangsan@163.com'
print(email.split('@'))

df = pd.read_csv('Airplane_Crashes.csv')
print(df.head())

print(df['Location'].str.split(',').head())
print(df['Location'].str.split(', ', expand=True).head())

# 使用n
print(df['Location'].str.split(', ', expand=True, n=1).head())

# 获取分割的结果
print(df['Location'].str.split(', ').str.get(0).head())
print(df['Location'].str.split(', ').str[1].head())

df = pd.DataFrame({'years': ['1900~2000', '1920~2020', '1930~2030']})
print(df['years'].str.split('~', expand=False).str[1])

