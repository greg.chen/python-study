import pandas as pd
import numpy as np

data = pd.read_csv('2016_happiness.csv', usecols=['Country', 'Region', 'Happiness Rank', 'Happiness Score'])
print(data.head())

# 使用apply函数，生成一列打分的整数部分列（四舍五入）
data['int_score'] =  data['Happiness Score'].apply(np.around)
print(data.head())

data.to_csv('2016_with_index.csv')
data.to_csv('2016_no_index.csv', index=False)