# -*- coding: utf-8 -*-

"""

    作者:     Robin
    版本:     1.0
    日期:     2019/04
    文件名:    main.py
    功能：     主程序

    实战：中国五大城市PM2.5数据分析

    任务：
        - 数据加载及合并
        - 数据预处理
        - 基于天数对比中国环保部和美国驻华大使馆统计的污染状态

    声明：小象学院拥有完全知识产权的权利；只限于善意学习者在本课程使用，
         不得在课程范围外向任何第三方散播。任何其他人或机构不得盗版、复制、仿造其中的创意，
         我们将保留一切通过法律手段追究违反者的权利
"""

import os
import matplotlib.pyplot as plt

import utils
import config


def main():
    """
        主函数
    """
    # 加载并预处理数据
    all_data_df = utils.load_and_process_data()

    # === 数据分析 ===
    # 通过分组操作获取每个城市每天的PM均值
    # 统计每个城市每天的平均PM2.5的数值
    day_stats = all_data_df.groupby(['city', 'date'])[['PM_China', 'PM_US Post']].mean()
    # 分组操作后day_stats的索引为层级索引['city', 'date']，
    # 为方便后续分析，将层级索引转换为普通列
    day_stats.reset_index(inplace=True)

    # 根据每天的PM值，添加相关的污染状态
    day_stats = utils.add_polluted_state_col_to_df(day_stats)
    # 基于天数对比中国环保部和美国驻华大使馆统计的污染状态
    comparison_result = utils.compare_state_by_day(day_stats)

    #  === 结果保存及展示 ===
    all_data_df.to_csv(os.path.join(config.output_path, 'all_cities_pm.csv'), index=False)
    day_stats.to_csv(os.path.join(config.output_path, 'day_stats.csv'))
    comparison_result.to_csv(os.path.join(config.output_path, 'comparison_result.csv'))

    # 可视化结果
    for city_name in config.data_config_dict.keys():
        city_comp_df = comparison_result.loc[:, comparison_result.columns.str.contains(city_name)]
        city_comp_df.plot(kind='bar')
        plt.tight_layout()
        plt.savefig('./output/{}.png'.format(city_name))
        plt.show()


if __name__ == '__main__':
    main()
