import pandas as pd

#Timedelta
print(pd.Timedelta('1 day'))
print(pd.Timedelta('1 day 2 hours'))
print(pd.to_timedelta(['1 day', '1 day 2 hours']))
print(pd.Timedelta('-1 day'))
#运算
s = pd.Series(pd.date_range('2019-1-1', periods=10, freq='D'))
td = pd.Series([pd.Timedelta(days=i) for i in range(10)])
df = pd.DataFrame({'A':s, 'B':td})
print(df)
print(df.info())

df['C'] = df['A'] + df['B']
df['D'] = df['A'] - df['B']
print(df)
print(df['C'] - df['A'])

#DateOffset
s2 = pd.Series(pd.date_range('2019-1', periods=6, freq='2M'))
df2 = pd.DataFrame({'A': s2})
print(df2)

# 1个月增量
df2['B'] = df2['A'] + pd.Timedelta('30 days')
df2['C'] = df2['A'] + pd.offsets.MonthEnd()
df2['D'] = df2['A'] + pd.offsets.WeekOfMonth(week=3)
print(df2)


