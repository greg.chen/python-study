import pandas as pd
import numpy as np
'''
分组：对数据集进行分组，然后对每组数据进行统计分析

分组运算的基本原理:

split->apply->combine

(1)拆分：进行分组的根据

(2)应用：每个分组进行的计算规则

(3)合并：把每个分组的计算结果合并起来

聚合：

数组产生标量的过程，如mean()、count()…

常用于对分组后的数据进行计算

内置的聚合函数：sum()，mean()，max()，count()
'''

'''
DataFrame可以进行两种方式的分组:

(1)按单列分组，obj.groupby(‘label’)

(2)按多列分组，obj.groupby([‘label1’,’label2’])，先按照label1分组，再按照label2分组

注意：

groupby()操作后仅产生GroupBy对象，而不进行实际运算，只有进行聚合操作后，才对分组后的数据进行聚合运算，常见的聚合操作:mean()，sum()，size()，count()…
'''

data = pd.read_csv("2016_happiness.csv", usecols=['Country', 'Region', 'Happiness Rank', 'Happiness Score'])
print(data.head())
obj1 = data.groupby('Region')
print(type(obj1))
print("obj1.mean()",obj1.mean())
print("obj1.max()",obj1.max())
print("obj1.size()",obj1.size())
print("obj1.count()",obj1.count())


df = pd.DataFrame({'key1':['a', 'c', 'b', 'a', 'b','b','c','a','b','c'],

     'key2':['one', 'three', 'two', 'two', 'one','three','three','two','one','one'],

     'data':np.random.randint(1,10,size=10)})

print(df.groupby("key1").max())