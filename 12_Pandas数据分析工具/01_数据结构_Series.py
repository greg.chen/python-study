import pandas as pd
import numpy as np

# 一个强大的分析结构化数据的工具集
#
# 基础是NumPy，提供了高性能矩阵的运算
#
# pd.Series数据结构：
#
# 1.构建Series数据：

print(pd.Series(range(10, 20)))

# 通过数组/列表
print("通过数组/列表",pd.Series(np.random.rand(5)))
# 通过dict
d = {'a' : 0., 'b' : 1., 'c' : 2.}
print("通过dict",pd.Series(d))
# 2.获取索引：
print("获取索引",pd.Series(d).index)
# ser_obj.index
#构建时指定索引
print("构建时指定索引",pd.Series(np.random.rand(5), index=['a','b','c','d', 'e']))

# 3.获取数据：
#
# 1>获取所有数据：ser_obj.values
ser_obj = pd.Series(np.random.rand(5))
print("head(1)",ser_obj.head(1))
print("index",ser_obj.index)
print("values",ser_obj.values)

ser_obj2 = pd.Series(np.random.rand(5), index=['a', 'b', 'c', 'd', 'e'])
# 通过索引名(字符串)获取数据
print("ser_obj2['b']",ser_obj2['b'])
# 通过in判断数据是否存在
print("通过in判断数据是否存在",'a' in ser_obj2)
# 通过索引位置(整型)获取数据
print(ser_obj2[0])

# 字符串缺失
countries = ['中国', '美国', '澳大利亚', None]
print("字符串缺失",pd.Series(countries))

ser_obj = pd.Series([1,3,4,6,7])

ser_obj.name='Age'
ser_obj.index.name= 'Index'
print(ser_obj)

ser_obj=pd.Series([1,3,5])
ser_obj.index[1]='b'
print(ser_obj)