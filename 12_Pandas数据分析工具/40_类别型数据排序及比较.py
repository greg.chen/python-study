import pandas as pd

data = ['a', 'b', 'c', 'a']

s = pd.Series(pd.Categorical(data, ordered=False))


# 设置ordered
from pandas.api.types import CategoricalDtype
s1 = s.astype(CategoricalDtype(categories=['c', 'b', 'a'], ordered=True))
print(s1.sort_values())

# 和标量比较
print(s1 > 'b')

s2 = s1.cat.reorder_categories(['b', 'c', 'a'], ordered=True)
print(s2.sort_values())

s3 = pd.Series(pd.Categorical(['a', 'c', 'b', 'a'],
                              categories=['c', 'b', 'a'],
                             ordered=True))

print(s3)


s1 = pd.Series(pd.Categorical(['a', 'c', 'b'], ordered=False))
s2 = pd.Series(pd.Categorical(['b', 'c', 'a'], ordered=True))
print(s1.sort_values())
print(s2.sort_values())

s1 = pd.Series(pd.Categorical(['a', 'c', 'b', 'a'], ordered=True))
s2 = pd.Series(pd.Categorical(['b', 'c', 'a', 'b'], ordered=True))
print(s1 > s2)