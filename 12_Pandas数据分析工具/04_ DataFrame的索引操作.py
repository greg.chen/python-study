
import pandas as pd
import numpy as np

# 构建DataFrame
country1 = pd.Series({'Name': '中国',
                    'Language': 'Chinese',
                    'Area': '9.597M km2',
                     'Happiness Rank': 79})

country2 = pd.Series({'Name': '美国',
                    'Language': 'English (US)',
                    'Area': '9.834M km2',
                     'Happiness Rank': 14})

country3 = pd.Series({'Name': '澳大利亚',
                    'Language': 'English (AU)',
                    'Area': '7.692M km2',
                     'Happiness Rank': 9})


df = pandas.DataFrame([country1,country2,country3], index=['CH', 'US', 'AU'])
#列索引
print("列索引",df['Area'])
#不连续索引
print("不连续索引",df[['Area', 'Name']])
# 行索引
print("行索引",df.loc['CH'])
print("行索引",df.iloc[1])
#混合索引
print("混合索引",df.loc['CH'][0])
print("混合索引",df.iloc[0]['Area'])
## 先列后行
print("先列后行",df['Area']['CH'])
# 删除Area列
df2 = df.drop('Area', axis=1)
print(df2)
# 写法2：inplace=True，在原数据上产生影响，返回None
df.drop('Area', axis=1, inplace=True)
print(df)

#布尔值遮罩
print("布尔值遮罩",df['Language'].str.contains('English'))
# 生成布尔值遮罩
filter_condition = df['Language'].str.contains('English')
print(" 生成布尔值遮罩",df[filter_condition])
# 过滤出排名前20的国家
print("过滤出排名前20的国家",df[df['Happiness Rank'] <= 20])

df_obj = pd.DataFrame([[1,2,3],[2,1,1],[4,3,1],[5,1,2]],columns=['a','b','c'])
#输出列名为”c”的列中元素大于1的所有列数据
print(df_obj)
print(df_obj.loc[df_obj.loc[:,"c"]>1,:])
print(df_obj.loc[:,(df_obj.loc[2,:]==1)&(df_obj.loc[3,:]==3)])

data_arr = np.array([np.random.randint(10) for i in range(20)]).reshape(5, 4)
df_obj = pd.DataFrame(data_arr,columns=['a','b','c',"d"])
print(df_obj)
#打印列名为”b”的数据，并且数据为Series结构类型
print(df_obj.loc[:,"b"])
print(df_obj.iloc[:,1])
print(df_obj.loc[0:2,['a','c']])
print(df_obj.iloc[0:3,[0,2]])
print(df_obj["b"])
print(df_obj.b)