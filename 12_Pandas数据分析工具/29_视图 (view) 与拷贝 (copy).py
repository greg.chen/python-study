import pandas as pd
import numpy as np

df1 = pd.DataFrame({'col1': [1, 2, 3, 4],
                   'col2': ['a', 'b', 'f', 'n'],
                   'col3': ['a', 'n', 'c', 'n']})


#链式索引取值
print(df1['col1'][0])

#链式索引赋值
# df1['col1'][0] = -10
# print(df1['col1'][0])

df2 = pd.DataFrame({'col1': [1, 2, 3, 4],
                   'col2': ['a', 'b', 'f', 'n'],
                   'col3': ['a', 'n', 'c', 'n']})

print(df2[df2['col1'] > 2])

df3 = pd.DataFrame({'col1': [1, 2, 3, 4],
                   'col2': ['a', 'b', 'f', 'n'],
                   'col3': ['a', 'n', 'c', 'n']})


df3.loc[0, 'col1'] = 10
print(df3.loc[0, 'col1'])

# 条件赋值
df4 = pd.DataFrame({'col1': [1, 2, 3, 4],
                   'col2': ['a', 'b', 'f', 'n'],
                   'col3': ['a', 'n', 'c', 'n']})

df4.loc[df4['col1'] > 2, 'col1'] = -1
print(df4)