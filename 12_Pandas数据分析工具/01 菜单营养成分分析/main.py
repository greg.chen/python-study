# -*- coding: utf-8 -*-

"""
    作者:     Robin
    版本:     1.0
    日期:     2019/04
    文件名:    main.py
    功能：     主程序

    实战：菜单营养成分分析

    任务：
        - 按类别卡路里过滤数据
        - 按单品类型分析查看数据
        - 为单品做标记健康标记，Y/N

    声明：小象学院拥有完全知识产权的权利；只限于善意学习者在本课程使用，
         不得在课程范围外向任何第三方散播。任何其他人或机构不得盗版、复制、仿造其中的创意，
         我们将保留一切通过法律手段追究违反者的权利
"""

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

import utils

# 数据文件地址
filename = './dataset/menu.csv'

# 分析的数据列
use_cols = ['Calories', 'Calories from Fat', 'Total Fat', 'Cholesterol', 'Sugars']


def main():
    """
        主函数
    """
    # 读入数据
    menu_data = pd.read_csv(filename)

    # 查看数据集基本信息
    utils.inspect_data(menu_data)

    # 任务1. 按类别卡路里过滤数据
    print('\n===================== 早餐大于400大卡的单品: =====================')
    fitered_item = utils.filter_by_cat_and_calories(menu_data, cat='Breakfast', cal=400)
    fitered_item_b = menu_data.loc[lambda df: (df['Category'] == 'Breakfast') & (df['Calories'] > 400)]


    print(fitered_item[['Item', 'Calories']].sort_values('Calories', ascending=False))

    # 任务2. 按单品类型分析查看数据
    utils.analyse_single_item(menu_data, use_cols)

    # print('\n===================== 营养成分最高的单品: =====================')
    # max_idxs = [menu_data[col].idxmax() for col in use_cols]
    # for col, max_idx in zip(use_cols, max_idxs):
    #     name = menu_data.at[max_idx, 'Item']
    #     value = menu_data.at[max_idx, col]
    #
    # print('\n===================== 营养成分最低的单品: =====================')

    # 任务3. 为单品做标记，将卡路里过高的单品标注为N，反之标注为Y
    proc_menu_data = utils.add_healthy_label(menu_data)
    # menu_data.loc[menu_data['Calories']> 400, 'Healthy'] = 'Y'
    # print(menu_data)

    sns.countplot(x='Healthy', data=proc_menu_data)
    plt.show()


if __name__ == '__main__':
    main()
