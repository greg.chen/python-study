# -*- coding: utf-8 -*-

"""
    作者:     Robin
    版本:     1.0
    文件名:    utils.py
    功能：     工具文件

    声明：小象学院拥有完全知识产权的权利；只限于善意学习者在本课程使用，
         不得在课程范围外向任何第三方散播。任何其他人或机构不得盗版、复制、仿造其中的创意，
         我们将保留一切通过法律手段追究违反者的权利
"""


def inspect_data(df_data):
    """
        查看加载的数据基本信息
    """
    print('数据集基本信息：')
    print(df_data.info())
    
    print('数据集有{}行，{}列'.format(df_data.shape[0], df_data.shape[1]))
    print('数据预览:')
    print(df_data.head())


def filter_by_cat_and_calories(menu_data, cat, cal):
    """
        按类别卡路里过滤数据，过滤属于cat类型且大于cal卡路里的产品
    """
    return menu_data.loc[lambda df: (df['Category'] == cat) & (df['Calories'] > cal)]


def analyse_single_item(df_data, cols):
    """
        按单品类型分析查看数据
    """
    print('\n===================== 营养成分最高的单品: =====================')
    # 获取每列的最大值对应的索引
    max_idxs = [df_data[col].idxmax() for col in cols]
    for col, max_idx in zip(cols, max_idxs):
        print('{} 最高的单品：{}，含量：{}'.format(col,
                                         df_data.at[max_idx, 'Item'],
                                         df_data.at[max_idx, col]))

    print('\n===================== 营养成分最低的单品: =====================')
    min_idxs = [df_data[col].idxmin() for col in cols]
    for col, min_idx in zip(cols, min_idxs):
        print('{} 最低的单品：{}，含量：{}'.format(col,
                                         df_data.at[min_idx, 'Item'],
                                         df_data.at[min_idx, col]))


def add_healthy_label(df_data, cal=400):
    """
        将卡路里过高(默认400)的单品标注为N，反之标注为Y，
    """
    df_data.loc[df_data['Calories'] < cal, 'Healthy'] = 'Y'
    df_data.loc[df_data['Calories'] >= cal, 'Healthy'] = 'N'
    return df_data
