import pandas as pd
import math
import numpy as np
# apply()函数

df = pd.DataFrame(np.arange(10).reshape(5, 2), columns=['col_1', 'col_2'])
print(df)
# 通过apply()将函数应用到行或列上
print(df.apply(np.sum))
# 注意参数axis
print(df.apply(np.sum, axis=1))

#
# 在DataFrame上操作时注意指定轴的方向，默认axis=0
#
# applymap()函数
#
# 通过applymap()将函数应用到对象的元素上，df的维度不变
print(df.applymap(np.sqrt))

staff_df = pd.DataFrame([{'姓名': '张三', '部门': '研发部'},
                        {'姓名': '李四', '部门': '财务部'},
                        {'姓名': '赵六', '部门': '市场部'}])
print("staff_df.iloc[1]",staff_df.loc[1])

# 获取姓
print(staff_df['姓名'].apply(lambda x: x[0]))
staff_df['姓'] = staff_df['姓名'].apply(lambda x: x[0])
print(staff_df)