import pandas as pd


pd.date_range(start='2019-01-01', end='2019-02-01')

data_df = pd.read_csv('BeijingPM20100101_20151231.csv')

data_df.index = pd.to_datetime(data_df[['year', 'month', 'day']])
print(data_df.head())
print(data_df.index)

print(data_df.truncate(before='2014'))

print(data_df.loc['20100101'].head())
print(data_df.loc['2015'].head())
print(data_df.index.year)
print(data_df.index.dayofyear)
print(data_df.index.month)

ser = pd.Series(pd.date_range(start='20190101', end='20190601'))
print(ser.dt.weekofyear)


dt1 = pd.date_range(start='20190101', periods=3)
dt2=pd.to_datetime('01/01/2019')
dt3=pd.to_datetime(['01/01/2019'])

print(type(dt1))
print(type(dt2))
print(type(dt3))
