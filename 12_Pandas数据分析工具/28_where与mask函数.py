import pandas as pd
import numpy as np

'''
where(cond, other)

返回符合条件cond的数据
返回数据的形状和原数据一致
不符合条件cond的位置填充other指定的值，other默认为nan

对比过滤操作


mask(cond, other=nan)
是 where() 的逆操作
将符合条件cond的值替换为other

'''

s = pd.Series(np.arange(5),
              index=['a', 'b', 'c', 'd', 'e'])

# 对比where与过滤操作
print(s[s>0])
print(s.where(s>0))
# 使用other参数
print('# 使用other参数', s.where(s>0, -1))

df = pd.DataFrame({'col1': [1, 2, 3, 4],
                   'col2': ['a', 'b', 'f', 'n'],
                   'col3': ['a', 'n', 'c', 'n']})

vals = {'col1': [1, 3],
         'col2': ['a', 'b']}

print('结合isin()', df.where(df.isin(vals), other=-1))

#mask
cond = s>0
print('mask', s.mask(cond))
print('~mask', s.mask(~cond))

df = pd.DataFrame({'Name':['Alisa','Bobby','Cathrine', 'Zoe'], 'Score':[62,85,89,70]})
cond = (df['Score'] >=60) & (df['Score'] <=70)
print(df['Score'].where(~cond, 60))
print(df['Score'].mask(cond, 60, inplace=True))
