import pandas as pd
import numpy as np

data = pd.DataFrame(
    {'A': [0, 1, 2, 3, 4],
    'B': [5, 6, 7, 8, 9],
    'C': ['a', 'b', 'c', 'd', 'e']}
)

print(data)

# 数值替换
print(data.replace(0, 100))
# 列表替换为值
print(data.replace([0, 1, 2, 3], 4))
# 列表替换为对应列表中的值
print(data.replace([0, 1, 2, 3], [4, 3, 2, 1]))
# 按字典替换
print(data.replace({0: 10, 1: 100}))