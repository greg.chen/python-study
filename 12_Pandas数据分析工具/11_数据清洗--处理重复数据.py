import pandas as pd
import numpy as np


data = pd.DataFrame(
    {'age': [28, 31, 27, 28],
    'gender': ['M', 'M', 'M', 'F'],
    'surname': ['Liu', 'Li', 'Chen', 'Liu']}
)

print(data)
#每行是否为重复行
print(data.duplicated())

print(data.duplicated(subset=['age', 'surname']))
# 过滤重复行，是对subset中的数据进行重复行过滤
# 默认判断全部列，可通过参数subset指定某些列
# keep，默认（first）保留第一次出现的数据，last表示保留重复行的最后一行
print(data.drop_duplicates(subset=['age', 'surname']))
print(data.drop_duplicates(subset=['age', 'surname'], keep='last'))

df_obj = pd.DataFrame(np.array([ [1,3,5],[3,np.nan,np.nan],[np.nan,2,3],[3,np.nan,np.nan],[2,np.nan,np.nan],[3,np.nan,np.nan] ]))
print(df_obj)
#对第二列和第三列的数据在水平方向上进行比较，判断是否存在重复行
print(df_obj.duplicated(subset=[1,2]))