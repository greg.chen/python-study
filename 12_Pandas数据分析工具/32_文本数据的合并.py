import pandas as pd
import numpy as np


s = pd.Series(['a', 'b', 'c', 'd'])

print(s.str.cat())
print(s.str.cat(sep='_'))

s2 = pd.Series(['A', 'B', 'C', 'D'])
print(s.str.cat(s2))
print(s.str.cat(s2,sep="-"))

# 合并列表
l = ['E', 'F', 'G', 'H']
print(s.str.cat(l, sep=', '))

arr = np.array(['I', 'J', 'K', 'L'])
print(s.str.cat(arr, sep=', '))

#同时合并多个对象
print(s.str.cat([s2,arr], sep='+'))

product_df = pd.DataFrame({'product': ['A', 'B', 'C'], 'price': [30, 20, 50]})
print(product_df['product'].str.cat(product_df['price'].astype(str), sep=', '))
print(product_df['product'].str.cat(sep=', '))