import pandas as pd
import numpy as np

'''
pd.concat(objs,axis)

含义：

按照指定轴的方向对对个数据对象进行数据合并。

参数：

objs：多个数据对象，如包含DataFrame的列表

axis：0按索引方向（纵向），1按列方向（横向）
'''

# 创建dataframe
df1 = pd.DataFrame({'A': ['A0', 'A1', 'A2', 'A3'],
                    'B': ['B0', 'B1', 'B2', 'B3'],
                    'C': ['C0', 'C1', 'C2', 'C3'],
                    'D': ['D0', 'D1', 'D2', 'D3']},
                    index=[0, 1, 2, 3])

df2 = pd.DataFrame({'A': ['A4', 'A5', 'A6', 'A7'],
                    'B': ['B4', 'B5', 'B6', 'B7'],
                    'C': ['C4', 'C5', 'C6', 'C7'],
                    'D': ['D4', 'D5', 'D6', 'D7']},
                    index=[4, 5, 6, 7])

df3 = pd.DataFrame({'C': ['A8', 'A9', 'A10', 'A11'],
                    'D': ['B8', 'B9', 'B10', 'B11'],
                    'E': ['C8', 'C9', 'C10', 'C11'],
                    'F': ['D8', 'D9', 'D10', 'D11']},
                    index=[0, 1, 2, 3])

# 列名相同
print("列名相同",pd.concat([df1, df2], axis=0))
# 列名不同
print("列名不同",pd.concat([df1, df3], axis=0))
# index相同
print("index相同",pd.concat([df1, df3], axis=1))
# index不同
print("index不同",pd.concat([df1, df2], axis=1))