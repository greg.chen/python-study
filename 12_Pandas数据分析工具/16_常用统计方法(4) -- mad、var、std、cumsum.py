import pandas as pd
import numpy as np

'''
mad()，求平均绝对误差（mean absolute deviation），对表示各个变量值之间差异程度的数值之一

var()：方差

std()：求标准差 = 方差开根号

cumsum()，求累加
'''

data = pd.read_csv('2016_happiness.csv')
data.head()

print("data.mad()",data.mad())
print("data.var()",data.var())
print("data.std()",data.std())
print('data.cumsum()',data.cumsum())

#计算ser=pd.Series([1,3,5,7,2])的方差
ser=pd.Series([1,3,5,7,2])
import math

def cul_var(ser):
    # 应用求和函数对ser对象求均值
    ser_mean = sum(ser)/len(ser)
    print(ser_mean)
    # 使用lambda,将ser中的每个元素减去ser_mean
    ser_sub_mean = pd.Series.apply(lambda x: x - ser_mean, ser)
    print(ser_sub_mean)
    # 将ser_sub_mean的每个元素计算平方
    er_sum = map((lambda x: x * x, ser))
    s = sum(er_sum) / len(ser)
    return s

print(cul_var(ser))
