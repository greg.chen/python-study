import pandas as pd
import numpy as np

'''
Merge连接：

merge根据单个或多个键将不同的DataFrame的行进行连接

merge(how,on,suffixes)

参数：

how：连接的方式，常见的连接方式有：inner（内连接），left（左连接），right（右连接），outer（全连接）

on：不同的DataFrame通过重叠列的列名作为“外键”进行连接

(1)on显示指定的“外键”；

(2)left_on可以指定左侧数据的“外键”，right_on可以指定右侧数据的“外键”，left_on和right_on多用于列的内容属于同一个类型，但是列名不同的情况；

(3)按索引进行连接，left_index=True或right_index=True

suffixes：处理重复列名，当指定了外键后，不同的DataFrame中仍然存在列名相同的列，默认情况下，相同列名的列会分别自动添加后缀_x,_y，也可以自定义设置后缀。
'''

# 创建dataframe
staff_df = pd.DataFrame([{'姓名': '张三', '部门': '研发部'},
                        {'姓名': '李四', '部门': '财务部'},
                        {'姓名': '赵六', '部门': '市场部'}])


student_df = pd.DataFrame([{'姓名': '张三', '专业': '计算机'},
                        {'姓名': '李四', '专业': '会计'},
                        {'姓名': '王五', '专业': '市场营销'}])

# 外连接
print("外连接",pd.merge(staff_df, student_df, how='outer', on='姓名'))
# 或者
# staff_df.merge(student_df, how='outer', on='姓名')
# 内连接
print("内连接",pd.merge(staff_df, student_df, how='inner', on='姓名'))
# 或者
# staff_df.merge(student_df, how='inner', on='姓名')

# 左连接
print("左连接",pd.merge(staff_df, student_df, how='left', on='姓名'))
# 或者
# staff_df.merge(student_df, how='left', on='姓名')

# 左连接
print("右连接",pd.merge(staff_df, student_df, how='right', on='姓名'))

# 添加新的数据列
staff_df['地址'] = ['天津', '北京', '上海']
student_df['地址'] = ['天津', '上海', '广州']

# 处理重复列名
# 如果两个数据中包含有相同的列名（不是要合并的列）时，merge会自动加后缀作为区别
print(pd.merge(staff_df, student_df, how='left', left_on='姓名', right_on='姓名'))

# 设置"姓名"为索引
staff_df.set_index('姓名', inplace=True)
student_df.set_index('姓名', inplace=True)

print(pd.merge(staff_df, student_df, how='left', left_index=True, right_index=True))