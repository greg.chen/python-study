import pandas as pd

'''
类别型数据为一个有限集合中的变量，如：
性别：男、女，血型：A、B、…，国籍：中国、美国…
Pandas中用category类型表示

构造方式：
创建变量时指定数据类型：dtype=‘category’，
对已有数据进行类型转换：astype(‘category’)
pd.cut()返回的结果
通过pd.Categorical()创建

describe() 输出信息：
count：数据个数
unique：类别个数
top：拥有最多数据的类别
freq：最多类别拥有数据的个数
'''

data = ['a', 'b', 'c', 'a']
# 不设置ordered
s = pd.Series(pd.Categorical(data, ordered=False))
print(s.sort_values())

from pandas.api.types import CategoricalDtype
s1 = s.astype(CategoricalDtype(categories=['c', 'b', 'a'], ordered=True))
print(s1)
print(s1.sort_values())
print(s1.max())
print(s1.min())

s2 = s1.cat.reorder_categories(['b', 'c', 'a'], ordered=True)
print("s2",s2.sort_values())


cat1= pd.Categorical(['a','b','c','a','b','c','d'])
print("cat1--->",cat1)
cat2= pd.Categorical(['a','b','c','a','b','c','d'], categories=['c', 'b', 'a'])
print("cat2--->",cat2)

cat = pd.Categorical(['a','b','c','a','b','c','d'], categories=['c', 'b', 'a'], ordered=True)
print(cat.max())

