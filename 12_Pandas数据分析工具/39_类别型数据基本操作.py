import pandas as pd

s = pd.Series(['a', 'b', 'c', 'a'], dtype='category')
print("s.cat.categories---->",s.cat.categories)
print(s.cat.ordered)
# 重新赋值cat.categories
s.cat.categories = ['类别1', '类别2', '类别3']
print(s)

print(s.cat.rename_categories(['cat1', 'cat2', 'cat3']))

s2 = s.cat.add_categories(['类别4', 5])
print(s2)

s3 = s2.cat.remove_categories([5])
print(s3)

s4 = s3.cat.remove_unused_categories()
print("remove_unused_categories-->",s4)

c = pd.Categorical(['a','b','c','a','b','c','d'], categories=['c', 'b', 'a'], ordered=True)
print(c.ordered)
print(c.remove_unused_categories().categories)
print(c.add_categories(['d']).remove_unused_categories().categories)
