
import pandas as pd
import numpy as np


log_data = pd.read_csv("log.csv")
print(log_data)

print(log_data.isnull())
print(log_data.isnull().any())
print(log_data.isnull().any(axis=1))
#丢弃缺失数据：dropna()
print(log_data.dropna())
#填充缺失数据：fillna(value)，以value填充数据
print(log_data.fillna(-1))

sorted_log_data = log_data.sort_values(by=['time', 'user'])
print(sorted_log_data)
#df.ffill()或者df.fillna(method=”ffill”)，按之前的数据填充
print(sorted_log_data.ffill())
#df.bfill()或者df.bfill(method=”bfill”)，按之后的数据填充
print(sorted_log_data.bfill())


df_obj = pd.DataFrame(np.array([[1,3,5],[np.nan,np.nan,3],[2,np.nan,np.nan],[np.nan,2,np.nan]]))
print(df_obj)
#对df_obj的每行进行后向填充，即水平方向上进行后填充。
print(df_obj.bfill(axis=1))

df_obj = pd.DataFrame(np.array([ [1,3,5],[np.nan,2,3],[2,8,np.nan],[np.nan,2,np.nan] ]))
#对df_obj中的NaN进行判断，然后得到不包含NaN值的所有列
print(df_obj.isnull())
print(df_obj.dropna(axis=1))