import pandas as pd


data_df = pd.read_csv('2016_happiness.csv', nrows=3)
print(data_df)

print('有序类别数据编码', data_df['Country'].map({'Denmark': 0, 'Switzerland': 1, 'Iceland': 2}))
print('无序类别数据编码', pd.get_dummies(data_df['Country']))

s1 = pd.Series(['A', 'B', 'C', 'D'])
s2 = pd.Series(['D', 'C', 'B', 'A'])
print(pd.get_dummies(s1))
print(pd.get_dummies(s2))