数据分析是指用适当的统计分析方法对收集来的大量数据进行分析，提取有用信息和形成结论而对数据加以详细研究和概括总结的过程。这一过程也是质量管理体系的支持过程。在实用中，数据分析可帮助人们作出判断，以便采取适当行动。
数据分析的数学基础在20世纪早期就已确立，但直到计算机的出现才使得实际操作成为可能，并使得数据分析得以推广。数据分析是数学与计算机科学相结合的产物。
在统计学领域，有些人将数据分析划分为描述性统计分析、探索性数据分析以及验证性数据分析；其中，探索性数据分析侧重于在数据之中发现新的特征，而验证性数据分析则侧重于已有假设的证实或证伪。
探索性数据分析是指为了形成值得假设的检验而对数据进行分析的一种方法，是对传统统计学假设检验手段的补充。该方法由美国著名统计学家约翰·图基(John Tukey)命名。
定性数据分析又称为“定性资料分析”、“定性研究”或者“质性研究资料分析”，是指对诸如词语、照片、观察结果之类的非数值型数据（或者说资料）的分析。