import pandas as pd
import numpy as np

'''
Python常用文本处理函数（字符串操作）
len(), split(), istitle(), endswith(), lower(), startswith()
in, upper(), titlecase(), splitlines(), join(), strip(), rstrip(), find(), rfind()
isupper(), islower(), replace()
isalpha(), isdigit(), isalnum()

'''

df = pd.read_csv('2016_happiness.csv')
# 全部转换为小写
print(df['Country'].str.lower().head())
# 求文本的长度
print(df['Country'].str.len().head())

df.columns = df.columns.str.lower()
print(df.columns)

s = pd.Series(['1.2', 'a', '12', 'One'])
print(s.str.isnumeric())

s=pd.Series(['  ', '\t\r', '\n', ''])
print(s.str.isspace())
