import pandas as pd
import numpy as np

#
# DataFrame数据结构：
# 类似多维数组/表格数据
# 每列数据可以是不同的类型
# 索引包括行索引(index)和列索引(label/column)
# DataFrame的构建以及操作：
#
# 1.构建DataFrame
# 通过ndarray/列表
array = np.random.randn(5, 4)
df_obj = pd.DataFrame(array)
print(df_obj)
# 通过dict
dict_data = {
    'A':1,
    'B':pd.Timestamp('20190101'),
    'C':pd.Series(1, index=list(range(4)), dtype='float32'),
    'D':np.array([3] * 4, dtype='int32'),
    'E': ['Python', 'Java', 'C++', 'C#'],
}
print(dict_data)
df_obj = pd.DataFrame(dict_data)
print(df_obj.columns)
print(df_obj.index)
print(df_obj.values)

print(df_obj.head(3))
print(df_obj.tail(3))
#增加列数据
df_obj['G'] = range(4)
print(df_obj)
#删除列
df_obj.drop(columns=['B', 'G'])
print(df_obj)
# 2.获取列数据（Series类型 ）
#
# df_obj[label] 或 df_obj.label
#
# 3.增加列数据
#
# df_obj[new_label] = data
#
# 4.删除列
#
# df_obj.drop(columns=[],inplace=False)，当inplace=False时，原数据不会改变，否则在原数据上发生改变。
# del df_obj[col_idx]


df=pd.DataFrame(np.array([[1.0,2.0,'a'],['g',3.0,4.0]]))

print(df.dtypes)

# 使用字典创建一个DataFrame的多维数组df_obj，Chinese列的值都是50，
# 对English和Chinese两列按行求均值，然后在原数据上删除English和Chinese两列，打印前3行数据。

dict_obj={"name":["LiLy","Lucy","LiLei","HanMei","Mr.Right"],
         "age":np.array([23,21,22,24,55]),
         "English":pd.Series([89,90,97,88,59]),
         "Chinese":50}

df_obj = pd.DataFrame(dict_obj)
df_obj["Mean score"]= (df_obj["English"]+df_obj["Chinese"])/2
df_obj.drop(["English","Chinese"],inplace=True,axis=1)
print(df_obj.head(3))
print(df_obj)