import pandas as pd
import numpy as np

raw_df = pd.read_csv('2016_happiness.csv')
print(raw_df.shape)

# 过滤前5行数据
proc_df = raw_df.iloc[:5, :]
print(proc_df)

# 随机采样
print("随机采样",raw_df.sample())
# 指定采样个数
print(proc_df.sample(n=2))
#指定采样比例
print('指定采样比例', proc_df.sample(frac=0.2))

#重复采样
print('重复采样', proc_df.sample(n=6, replace=True))

#使用随机种子
print('使用随机种子', proc_df.sample(random_state=1))
#在列方向随机采样
print("在列方向随机采样",proc_df.sample(axis=1))