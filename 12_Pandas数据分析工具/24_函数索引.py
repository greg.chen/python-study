import pandas as pd
import numpy as np


raw_df = pd.read_csv('2016_happiness.csv')
print(raw_df.head())

#过滤排名在10-20的国家和排名
print("过滤排名在10-20的国家和排名",
      raw_df.loc[lambda df: (df['Happiness Rank'] >= 10) & (df['Happiness Rank'] <= 20), ['Country', 'Happiness Rank']])

#过滤排名前5行数据的国家和排名
print("* 过滤排名前5行数据的国家和排名", raw_df.iloc[:5, lambda df:[0,2]])

#* 获取幸福指数平均得分大于7的地区
print("* 获取幸福指数平均得分大于7的地区", raw_df.groupby('Region')['Happiness Score'].mean().loc[lambda s:s>7])

df=pd.DataFrame({'col1': [1, 2, 3], 'col2': [4, 5, 6]})
print(df['col1'].iloc[lambda x: x[[0, 1]]])
print(df.loc[lambda x: (x['col1'] > 1) & (x['col2'] < 6)])