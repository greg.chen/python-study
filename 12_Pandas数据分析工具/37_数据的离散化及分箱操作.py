import pandas as pd

'''
pandas.cut(x, bins, labels)
x: 被分箱的数据，一维数据
bins:
整型：分箱的个数
列表：bin的边界
labels: 分箱后每个bin的名称

'''

df = pd.DataFrame({'Name':['George','Andrea','micheal','maggie','Ravi','Xien','Jalpa','Tyieren'],
                    'Score':[63,48,56,75,32,77,85,22],
                    'Score1':[63,48,56,75,32,77,85,22]
                   })

print(pd.cut(df['Score'], bins=3))

print(pd.cut(df['Score'], bins=[0, 25, 50, 75, 100]))

# 对得分进行分箱操作，指定labels
print(pd.cut(df['Score'], bins=3, labels=['C', 'B', 'A']))

df['Level'] = pd.cut(df['Score'], bins=3, labels=['C', 'B', 'A'])
print(df.groupby('Level').mean())

bins=[1, 2, 3, 4, 5]
print(pd.cut([0, 1, 1.5, 2.5, 3.5], bins))