import pandas as pd


#通过日期型数据列表创建
from datetime import datetime

date_list1 = [datetime(2019, 1, 1), datetime(2019, 1, 2), datetime(2019, 1, 3)]

print(pd.Series(date_list1))

date_list2 = [pd.Timestamp('20190101'),pd.Timestamp('20190102'),pd.Timestamp('20190103')]
print(pd.Series(date_list2))

date_list3 = [pd.Timestamp('2019/01/01'), pd.Timestamp('2019/01/02'), pd.Timestamp('2019/01/03')]
print(pd.Series(date_list3))

print(pd.Series(pd.date_range(start='2019-01-01', end='2019-02-01')))
print(pd.Series(pd.date_range(start='2019/01/01', periods=10)))
print(pd.Series(pd.date_range(start='20190101', periods=6, freq='M')))

date_val_list = ['2019-01-01', '20190102', None]
print(pd.Series(pd.to_datetime(date_val_list)))

df = pd.read_csv('BeijingPM20100101_20151231.csv')
print(df.head())
print(pd.to_datetime(df[['year','month','day']]))

print(pd.Series(pd.date_range(start='20190101', end='2019-01-05', periods=3, freq='2D')))