import pandas as pd
import numpy as np

'''
max()，求最大值
min()，求最小值
idxmax()，返回最大值对应的索引
idxmin()，返回最小值对应的索引
'''

data = pd.read_csv('2016_happiness.csv')
data.head()

print(data.max())
print(data.min())

print(data['Happiness Score'].idxmax())
print(data['Happiness Score'].idxmin())


df_obj = pd.DataFrame({"A":[1,3,4,9],"B":[np.nan,8,np.nan,5],"C":[9,np.nan,2,10]})
print(df_obj)
print(df_obj.idxmin(skipna=True))
print(df_obj.idxmin(skipna=True))

df_obj = pd.DataFrame({"A":[1,3,4,9],"B":[np.nan,8,np.nan,5],"C":[9,np.nan,2,10]})
print(df_obj)
print(df_obj.min(skipna=True))