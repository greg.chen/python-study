import pandas as pd
import numpy as np

'''
透视表(pivot table)介绍

(1)计算、汇总和分析数据的强大工具

(2)包括分组、统计操作

(3)可以将“扁平”的表转换为“立体”表的过程
'''

# 创建dataframe
d = {
    'Name': ['Alisa', 'Bobby', 'Cathrine', 'Alisa', 'Bobby', 'Cathrine',
             'Alisa', 'Bobby', 'Cathrine', 'Alisa', 'Bobby', 'Cathrine'],

    'Semester': ['Semester 1', 'Semester 1', 'Semester 1', 'Semester 1', 'Semester 1', 'Semester 1',
                 'Semester 2', 'Semester 2', 'Semester 2', 'Semester 2', 'Semester 2', 'Semester 2'],

    'Subject': ['Mathematics', 'Mathematics', 'Mathematics', 'Science', 'Science', 'Science',
                'Mathematics', 'Mathematics', 'Mathematics', 'Science', 'Science', 'Science'],
    'Score': [62, 47, 55, 74, 31, 77, 85, 63, 42, 67, 89, 81]}

df = pd.DataFrame(d)
print(df)
print(df.groupby(by=['Semester', 'Subject'])['Score'].mean().to_frame())

'''
透视表

df.pivot_table(values,index,columns,aggfunc,margins)

含义：

根据一个或多个键对数据进行聚合，并根据行和列上得分组建将数据分配到各个矩形区域中。

参数：

values：透视表中的元素值(根据聚合函数得出)

index：透视表的行索引

columns：透视表的列索引

aggfunc：聚合函数，可以指定多个函数

margins：表示是否对所有数据进行统计
'''

# 创建dataframe
d = {
    'Name': ['Alisa', 'Bobby', 'Cathrine', 'Alisa', 'Bobby', 'Cathrine',
             'Alisa', 'Bobby', 'Cathrine', 'Alisa', 'Bobby', 'Cathrine'],

    'Semester': ['Semester 1', 'Semester 1', 'Semester 1', 'Semester 1', 'Semester 1', 'Semester 1',
                 'Semester 2', 'Semester 2', 'Semester 2', 'Semester 2', 'Semester 2', 'Semester 2'],

    'Subject': ['Mathematics', 'Mathematics', 'Mathematics', 'Science', 'Science', 'Science',
                'Mathematics', 'Mathematics', 'Mathematics', 'Science', 'Science', 'Science'],
    'Score': [62, 47, 55, 74, 31, 77, 85, 63, 42, 67, 89, 81]}

df = pd.DataFrame(d)
print(df)
print(df.pivot_table(values='Score', index='Semester', columns='Subject', aggfunc={'mean', 'max', 'min'}))
