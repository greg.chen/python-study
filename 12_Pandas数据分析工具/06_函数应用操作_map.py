import pandas as pd
import math
import numpy as np

# map(function,series)
# 作用：将funciton函数作用于一个Series的每一个元素
l = list(range(10))
print(l)

res = map(math.sqrt, l)
# 注意直到取出结果，才会执行操作
print(list(res))



# Series：是待分析的数据
# function函数：可以是NumPy中的通用函数(np.max、np.mean等)
# 可以是自定义函数(def function)
# 优点：不使用循环，代码简洁，效率高

ser = pd.Series(l)
print(ser)

# 开根号
print(ser.map(np.sqrt))
print(ser.map(lambda x:x**2 + 1))

ser = pd.Series(list("abcbd"))
print(ser)

print(ser.map(lambda x:".".join([x,'com'])))


lst = [1,2,3,5]
print(list(map(lambda x:x**2,lst)))