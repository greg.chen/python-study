import pandas as pd
import numpy as np

data_df = pd.read_csv('2016_happiness.csv')

s = pd.Series(['1. Ant.  ', '2. Bee!\n', '3. Cat?\t', np.nan])
print('s.str.len()', s.str.len())
print('s.str.strip()', s.str.strip())
print('s.str.strip().str.len()', s.str.strip().str.len())

# 区分大小写
print('# 区分大小写', data_df['Region'].str.contains('europe').head(10))
# 数据过滤
print('# 数据过滤', data_df[data_df['Region'].str.contains('europe', case=False)].head())
print('# 数据过滤', data_df[lambda  df: df['Region'].str.contains('europe', case=False)].head())

s = pd.Series(['a', 'b', 'c'])
# 每个数据重复相同次数
print("每个数据重复相同次数",s.str.repeat(repeats=2))
# 每个数据重复不同的次数
print('# 每个数据重复不同的次数', s.str.repeat(repeats=[1, 2, 3]))

# 找出以Z开头的国家记录
print('# 找出以Z开头的国家记录', data_df[data_df['Country'].str.startswith('Z')].head())
print('变换字母大小写',data_df['Country'].str.swapcase().head())

data_df2 = pd.DataFrame()
data_df2['col1'] = ['aBc', 'abc', 'ABC']
data_df2['col2'] = ['1212.123', 'abc', '2323']
data_df2['col1'].str.islower()
data_df2['col1'].str.isupper()
data_df2['col2'].str.isnumeric()

s = pd.Series(['dog', '', 5, None])
print(s.str.len())