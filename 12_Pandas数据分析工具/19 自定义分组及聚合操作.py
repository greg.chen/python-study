import pandas as pd
import numpy as np

'''
1、自定义分组：

(1)groupby()可以传入自定义的函数进行分组，操作针对的是索引

(2)可以通过自定义函数构造出一个分组列，然后根据分组列进行groupby

2、自定义聚合操作：

(1)使用agg()函数

(2)传入包含多个函数的列表，可同时完成多个聚合操作

(3)可通过字典为每个列指定不同的操作方法

(4)传入自定义函数
'''

data = pd.read_csv("2016_happiness.csv", usecols=['Country', 'Region', 'Happiness Rank', 'Happiness Score'])

print(data.head())

# 自定义分组规则
def get_score_group(score):
    if score <= 4:
        score_group = 'low'
    elif score <= 6:
        score_group = 'middle'
    else:
        score_group = 'high'
    return score_group

# 方法1：传入自定义的函数进行分组按单列分组
data2 = data.set_index('Happiness Score')
print(data2.groupby(get_score_group).size())

# 方法2：人为构造出一个分组列
data['score group'] = data['Happiness Score'].apply(get_score_group)
print(data.tail())
print(data.groupby('score group').size())

data = pd.read_csv("2016_happiness.csv", usecols=['Country', 'Region', 'Happiness Rank', 'Happiness Score'])

print(data.groupby('Region').max())
print(data.groupby('Region').agg(np.max))

# 传入包含多个函数的列表
print(data.groupby('Region')['Happiness Score'].agg([np.max, np.min, np.mean]))

# 通过字典为每个列指定不同的操作方法
print(data.groupby('Region').agg({'Happiness Score': np.mean, 'Happiness Rank': np.max}))

# 传入自定义函数
def max_min_diff(x):
    return x.max() - x.min()

print(data.groupby('Region')['Happiness Rank'].agg(max_min_diff))

