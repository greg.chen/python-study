import pandas as pd
import matplotlib.pyplot as plt

data_df = pd.read_csv('day_stats.csv', index_col='date', parse_dates=True, dayfirst=True)
print(data_df.head())

beijing_data = data_df[data_df['city'] == 'beijing'].copy()

beijing_data['PM_China'].plot(figsize=(10, 5))
print("beijing_data.shift(1)",beijing_data.shift(1))

#位移 shift
beijing_data['+1'] = beijing_data['PM_China'].shift(1)
beijing_data['-1'] = beijing_data['PM_China'].shift(-1)
print(beijing_data.head())


beijing_data.loc['2015-01'][['PM_China', '+1', '-1']].plot(figsize=(10, 5))
plt.show()

#频率调整 asfreq
print(beijing_data.asfreq(freq='M').head())

# 低频转高频
print(beijing_data.asfreq(freq='12H').head(20))

print(beijing_data.asfreq(freq='12H', fill_value=0).head(20))