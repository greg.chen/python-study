import pandas as pd
import numpy as np

#Series间运算
ser1 = pd.Series(range(10,20),index = range(10))
print(ser1)
ser2 = pd.Series(range(10,20),index = range(10))
print("ser2",ser2)
print(ser1 + ser2)

#DataFrame间的运算
#对齐操作会同时发生在行和列上，注意：Series和DataFrame操作时，
# Series被看作行数据（index被看作列），和DataFrame中的每行数据进行计算。

df1 = pd.DataFrame(np.ones((2,2)), columns=['a', 'b'])
print("df1",df1)
df2 = pd.DataFrame(np.ones((3,3)), columns=['a', 'b','c'])
print("df2",df2)
#      a    b   c
# 0  2.0  2.0 NaN
# 1  2.0  2.0 NaN
# 2  NaN  NaN NaN
print(df1+ df2)

#Series和DataFrame的运算
#索引与数据的对应关系仍保持在数组运算的结果中。
#若是没有对齐的位置，则运算结果位置默认补NaN

print("ser2 + df1",ser2 + df1)

ser3 = pd.Series([1, 2, 3], index=['a', 'b', 'c'])
print("ser3",ser3)
print("ser3 + df1",ser3 + df1)



df1=pd.DataFrame([[1,1,1]]*3)
print(df1)
ser1 = pd.Series([10,20,30,40,50,60])
print(ser1)
print("df1 +ser1",df1 +ser1)
print(df1.add(ser1))

ser1 = pd.Series([10,20,30,40,50,60])

ser2 = pd.Series([1,2,3])

print("ser1.add(ser2,fill_value=1)",ser1.add(ser2,fill_value=1))