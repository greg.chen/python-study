import pandas as pd
import numpy as np


# 常用统计方法：
#
# sum()，求和
#
# mean()，求均值
#
# median()，求中位数
#
# count()，求非空的个数
#
'''注意：以上统计方法不对缺失数据进行统计'''

data = pd.read_csv('log.csv')
data.head()

print("sum",data.sum())
print("mean", data.mean())
print("count", data.count())

df_obj = pd.DataFrame([[1,3,np.nan],[np.nan,3,9],[np.nan,1,5],[3,5,2]])
print(df_obj)
print(df_obj.median(axis=1))