import pandas as pd
import numpy as np

# 按索引排序：sort_index()
#
# ascending：默认为True，True 升序，False 降序
#
# axis：默认为0，0表示按行索引标签，即垂直方向上；
#
# 1表示按列索引标签，即水平方向上。
#
# 对DataFrame操作时注意轴方向
#
# 按值排序：sort_values(by, ascending)
#
# 1.按单列的值排序
#
# axis：默认为0，0表示按列索引，即垂直方向上；1表示按行索引，即水平方向上。
#
# by=‘label’，by的值选取，与axis的轴方向有关系，当axis=0，表示从列索引的标签中选取标签，axis=1时，表示从行索引的标签中选取标签。
#
# ascending：默认为True，True 升序，False 降序
#
# 2.按多列的值排序，by=[  ]，ascending = [ ]，by的长度必须和ascending的长度必须一致，且是一一对应的关系。

data = pd.read_csv('2016_happiness.csv',
                   usecols=['Country', 'Region', 'Happiness Rank', 'Happiness Score'],
                   index_col='Happiness Rank')

print(data.head(5))


# 默认 axis=0，表示按行排序
print(data.sort_index(ascending=False).head(5))

print(data.sort_index(axis=1).head(5))

#按值排序
print(data.sort_values(by='Country').head(10))

# 按多列排序
print(data.sort_values(by=['Region', 'Country']).head(10))

# 按多列排序
print(data.sort_values(by=['Region', 'Country'], ascending=[True, False]).head(10))


df_obj = pd.DataFrame(np.array([1,4,2,4,6,7,4,2,4,6,2,1,3,5,9,3]).reshape(4,4))
#对df_obj数据按照第三列的元素大小进行升序排序
print(df_obj.sort_values(by=2, ascending=True))
print(df_obj.sort_values(by=2,axis=0))

df_obj = pd.DataFrame(np.array([1,4,2,4,6,7,4,2,4,6,2,1,3,5,9]).reshape(5,3),columns=['A','B','C'])
df_obj.index = ['a','c','d','b','e']
print(df_obj.sort_index(ascending=False))