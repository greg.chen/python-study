import pandas as pd
import numpy as np

'''
数据重构：stack和unstack

stack()和unstack()适用于层级索引对象，仅是对数据显示的转换，并不对数据本身产生聚合操作。

1> stack(level=n)：将数据的列索引旋转为行索引，n指索引的层级，默认为-1，最里层的索引为-1或者(层级数-1)，Level从外往里依次为：0,1…-1

2> unstack(level=n)：将数据的行索引旋转为列索引。
'''

# 创建dataframe
header = pd.MultiIndex.from_product([['Semester1', 'Semester2'], ['Maths', 'Science']])
d = [[12, 45, 67, 56], [78, 89, 45, 67], [45, 67, 89, 90], [67, 44, 56, 55]]

df = pd.DataFrame(d, index=['Alisa', 'Bobby', 'Cathrine', 'Jack'], columns=header)
print(df)

stacked_df = df.stack()
print(stacked_df)

# level 参数
print(df.stack(level=0))
print(df.stack(level=1))
print(df.stack(level=-1))

print(stacked_df.unstack())

df = pd.DataFrame(np.random.randint(0,150,size=(4,4)),

               index = ['第一季度','第二季度','第三季度','第四季度'],

               columns=[['python','python','机器学习','机器学习'],['初级','高阶','初级','高阶']])
print(df)
stacked_df=df.stack()
stacked_sl_df = stacked_df.swaplevel()
print(stacked_sl_df.unstack(-1))