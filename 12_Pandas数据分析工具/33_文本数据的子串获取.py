import pandas as pd
import numpy as np


data_df = pd.read_csv('2016_happiness.csv')
data_df.head()

# 获取Country列中第1个位置的字符
print('获取Country列中第1个位置的字符', data_df['Country'].str.get(1).head())

# str.slice()
print('str.slice()', data_df['Country'].str.slice(1, 3).head())
print('str.slice()', data_df['Country'].str.slice(0, -1, 2).head())

s = pd.Series(['2019-01-01', '2018/01/01', '2017年1月1日'])
print(s.str[:4])
print(s.str.slice(0,4))
