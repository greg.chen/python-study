from wordcloud import WordCloud
import matplotlib.pyplot as plt

with open('eng.txt', 'r') as f:
    eng_text = f.read()

print(eng_text)

wordcloud = WordCloud().generate(eng_text)
plt.imshow(wordcloud, interpolation='bilinear')
plt.axis('off')
plt.show()

with open('ch.txt', 'r', encoding='utf-8') as f:
    ch_text = f.read()


import jieba

words = jieba.cut(ch_text)
words_str = ' '.join(words)

wordcloud = WordCloud(font_path='./fonts/simhei.ttf').generate(words_str)
plt.imshow(wordcloud, interpolation='bilinear')
plt.axis('off')
plt.show()

