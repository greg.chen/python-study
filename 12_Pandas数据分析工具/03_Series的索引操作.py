import pandas as pd
import numpy as np

ser_obj = pd.Series(range(5), index=['a', 'b', 'c', 'd', 'e'])
print(ser_obj)
#行索引
print(ser_obj['b'])
print(ser_obj.loc['b'])
print(ser_obj[1])
print(ser_obj.iloc[1])

#切片索引
print(ser_obj[1:3])
print("ser_obj['b':'d']",ser_obj['b':'d'])

#不连续索引
print(ser_obj[[0, 2, 4]])
print(ser_obj[['b', 'd']])


ser_obj = pd.Series([2,4,5,9,1,6])
ser_obj.index = ['a','b','c','d','e','f']
print("ser_obj[ser_obj > 3]",ser_obj[ser_obj > 3])