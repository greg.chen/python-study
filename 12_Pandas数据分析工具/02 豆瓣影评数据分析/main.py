# -*- coding: utf-8 -*-

"""
    作者:     Robin
    版本:     1.0
    日期:     2019/04
    文件名:    main.py
    功能：     主程序

    实战：豆瓣影评数据分析

    任务：
        - 数据预处理及分词
        - 词云分析

    声明：小象学院拥有完全知识产权的权利；只限于善意学习者在本课程使用，
         不得在课程范围外向任何第三方散播。任何其他人或机构不得盗版、复制、仿造其中的创意，
         我们将保留一切通过法律手段追究违反者的权利
"""

# 引入必要的包
import pandas as pd
import os

import utils


# 指定数据集路径
# 原始数据
datafile = './dataset/movie_reviews.csv'

# 预处理后的数据
cln_datafile = './reviews_cln_data.csv'


def main():
    """
        主函数
    """
    # 任务1. 数据预处理及分词
    if not os.path.isfile(cln_datafile):
        # 加载数据
        raw_data = pd.read_csv(datafile)

        # 数据查看
        print('\n===================== 数据查看 =====================')
        utils.inspect_data(raw_data)

        print('\n===================== 任务1. 数据预处理及分词 =====================')
        cln_data = utils.preprocess_data(raw_data)
        # 将处理后的数据集保存
        cln_data.to_csv(cln_datafile, encoding='utf-8', index=False)

    # 任务2. 词云分析
    print('\n===================== 任务2. 词云分析 =====================')
    cln_data = pd.read_csv(cln_datafile)
    utils.generate_wordcloud('复仇者联盟', cln_data)
    utils.generate_wordcloud('小时代', cln_data)


if __name__ == '__main__':
    main()
