# -*- coding: utf-8 -*-

"""
    作者:     Robin
    版本:     1.0
    文件名:    utils.py
    功能：     工具文件

    声明：小象学院拥有完全知识产权的权利；只限于善意学习者在本课程使用，
         不得在课程范围外向任何第三方散播。任何其他人或机构不得盗版、复制、仿造其中的创意，
         我们将保留一切通过法律手段追究违反者的权利
"""
import matplotlib.pyplot as plt
from wordcloud import WordCloud
import jieba

# 解决matplotlib显示中文问题
# 仅适用于Windows
plt.rcParams['font.sans-serif'] = ['SimHei']  # 指定默认字体
plt.rcParams['axes.unicode_minus'] = False  # 解决保存图像是负号'-'显示为方块的问题


def inspect_data(df_data):
    """
        查看加载的数据基本信息
    """
    print('数据集基本信息：')
    print(df_data.info())
    
    print('数据集有{}行，{}列'.format(df_data.shape[0], df_data.shape[1]))
    print('数据预览:')
    print(df_data.head())

    # 电影名称
    print('数据集包含{}部电影。'.format(len(df_data['Movie_Name_CN'].unique())))
    print(df_data['Movie_Name_CN'].unique())

    # 电影平均得分
    movie_mean_score = df_data.groupby('Movie_Name_CN')['Star'].mean().sort_values(ascending=False)
    movie_mean_score.plot(kind='bar')
    plt.tight_layout()
    plt.show()


def preprocess_data(df_data):
    """
        数据预处理
    """
    # 去除空值（如果有的话）
    df_data.dropna(inplace=True)

    # 处理数据集中的所有文本
    df_data['Words'] = df_data['Comment'].apply(proc_text)

    df_data.dropna(subset=['Words'], inplace=True)

    # 数据预览
    print('预处理后的数据预览：')
    print(df_data.head())

    return df_data[['Movie_Name_CN', 'Words']]


def proc_text(raw_line):
    """
        处理文本数据
        返回分词结果
    """
    words = jieba.cut(raw_line)
    return ' '.join(words)


def generate_wordcloud(movie_name, cln_data):
    """
        根据电影名称生成词云
    """
    reviews = cln_data[cln_data['Movie_Name_CN'].str.contains(movie_name)]['Words'].str.cat(sep=' ')
    wordcloud = WordCloud(font_path='./fonts/simhei.ttf').generate(reviews)
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis('off')
    plt.tight_layout()
    plt.savefig('./output/{}_wc.png'.format(movie_name))
    plt.show()


