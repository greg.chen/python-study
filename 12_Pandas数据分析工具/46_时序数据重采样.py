import pandas as pd
import numpy as np

data_df = pd.read_csv('day_stats.csv', index_col='date', parse_dates=True, dayfirst=True)
print(data_df.head())

beijing_data = data_df[data_df['city'] == 'beijing'].copy()

#降采样
month_sum_sample = beijing_data['PM_China'].resample('M').sum()
month_mean_sample = beijing_data['PM_China'].resample('M').mean()
month_ohlc_sample = beijing_data['PM_China'].resample('M').ohlc()

print(month_sum_sample)
print(month_mean_sample)
print(month_ohlc_sample)

#升采样
df = pd.DataFrame(np.random.randn(5, 3),
                 index=pd.date_range('20190101', periods=5, freq='W-MON'), # 	weekly frequency (mondays)
                 columns=['S1', 'S2', 'S3'])
print(df.head())

# 直接重采样会产生空值
print(df.resample('D').asfreq())

# ffill ( Limit of how many values to fill.)
print(df.resample('D').ffill(2))
# bfill
print(df.resample('D').bfill())

# 线性插值
print(df.resample('D').interpolate('linear'))

