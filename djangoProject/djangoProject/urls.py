"""djangoProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,re_path,include
from app01 import urls as app01_urls
from app02 import urls as app02_urls


urlpatterns = [
    path('admin/', admin.site.urls),
    #写自己的路由与视图
    # path(r'app01/',include(app01_urls)), # url 前缀是app01开头
    # path(r'app02/', include(app02_urls)),
    #终极写法
    path('app01/', include(('app01.urls'))),
    path('app02/', include(('app02.urls')))
    # path('app01/', include(('app01.urls','app01'), namespace='app01')),
    # path('app02/', include(('app02.urls','app02'), namespace='app02'))
]
