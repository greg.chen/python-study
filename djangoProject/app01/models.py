from django.db import models

# 数据库迁移命令
# python3 manage.py makemigrations
# python3 manage.py migrate

# Create your models here.
class User(models.Model):
    # id int primary_key auto_increment
    id = models.AutoField(primary_key=True)
    #CharField 必须指定max_length
    username = models.CharField(max_length=32, verbose_name='用户名')
    password = models.CharField(max_length=64, verbose_name='密码')
    #age = models.IntegerField(verbose_name='年龄')
    # 该字段可以为空
    info = models.CharField(max_length=32, verbose_name='个人简介', null=True)
    # 直接设置默认值
    # hobby = models.CharField(max_length=32, verbose_name='兴趣爱好', default='test')
    register_time = models.DateField(auto_now_add=True, null=True)

    def __str__(self):
        return '%s'% self.username



class Book(models.Model):
    title = models.CharField(max_length=32)
    # 小数总共8位, 小数点后面占2位
    price = models.DecimalField(max_digits=8, decimal_places=2)
    # 默认与Publish主键关联 1对多
    # 图书和出版社1对多, 并且书是多的一方, 所以外键字段放在书表里
    # 如果是外键, 会自动加_id(publish_id)
    publish = models.ForeignKey(to="Publish", on_delete=models.CASCADE)
    # 图书与作者多对多
    # authors虚拟字段 主要是告诉ORM书籍表和作者表是多对多的关系, orm自动创建第三张表
    authors = models.ManyToManyField(to="Author")



class Publish(models.Model):
    name = models.CharField(max_length=32)
    addr = models.CharField(max_length=32)


class Author(models.Model):
    name = models.CharField(max_length=32)
    age = models.IntegerField()
    # 作者与作者详情 1对1的关系, 外键建在任一方都可以.
    author_detail = models.OneToOneField(to="AuthorDetail", on_delete=models.CASCADE)


class AuthorDetail(models.Model):
    phone = models.BigIntegerField()
    addr = models.CharField(max_length=32)
