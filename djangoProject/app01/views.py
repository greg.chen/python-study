from django.shortcuts import render, redirect, HttpResponse,reverse
from app01 import models


# Create your views here.
def index(request, *args, **kwargs):
    print(args,kwargs)
    return render(request, "01/myfirst.html")


def home(request):

    return render(request, 'home.html')

import json
from django.http import JsonResponse

def ab_json(request):
    user_dict = {'username':'打一枪', 'password':'123'}
    #json_str = json.dumps(user_dict, ensure_ascii=False)

    return JsonResponse(user_dict)


def ab_file(request):
    if request.method == 'GET':
        return render(request, "ab_file.html")
    elif request.method == 'POST':
        file_obj = request.FILES.get('file')
        with open(file_obj.name,'wb') as f:
            for line in file_obj.chunks():
                f.write(line)

        return HttpResponse("上传成功")

def login(request):
    if request.method == 'GET':
        return render(request, "login.html")
    elif request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user_obj = models.User.objects.filter(username=username).first()
        # print(user_obj.username)
        # print(user_obj.password)
        if user_obj:
            if password == user_obj.password:
                return HttpResponse("登录成功")
            else:
                return HttpResponse("登录失败")

        return HttpResponse("收到 %s %s" % (username, password))


def reg(request):
    if request.method == 'GET':
        return render(request, "reg.html")
    elif request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        confirm_password =  request.POST.get('confirm_password')

        #第一种
        # res = models.User.objects.create(username=username, password=password)
        # 第二种
        user_obj = models.User(username=username, password=password)
        user_obj.save()
        print(user_obj)
        return HttpResponse("注册成功")


def userlist(request):
    # 查询所有数据
    #data = models.User.objects.filter()
    user_queryset = models.User.objects.all()

    print(user_queryset)
    #return render(request, 'userlist.html', {'user_queryset': data})
    return render(request, 'userlist.html', locals())


def edit_user(request):
    user_id = request.GET.get('user_id')
    user_obj = models.User.objects.filter(id=user_id).first()

    if request.method == 'GET':

        print(user_obj)
        return render(request, "edit_user.html", locals())
    elif request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        # edit_obj = models.User.objects.filter(id=user_id).update(username=username, password=password)

        user_obj.username = username
        user_obj.password = password
        user_obj.save()

        return redirect("/userlist/")

def delete_user(request):
    user_id = request.GET.get('user_id')
    user_obj = models.User.objects.filter(id=user_id).first()
    user_obj.delete()
    return redirect("/userlist/")


from django.views import View

class MyLogin(View):

    def get(self, request):
        return HttpResponse("get方法")

    def post(self, request):
        return HttpResponse("post方法")