from django.urls import path,re_path
from app01 import views

urlpatterns = [
    # #写自己的路由与视图
    # re_path(r'index/(\d+)',views.index),
    # path('index2/<int:year>/<int:month>/',views.index),
    path(r'home/', views.home, name='app01_home'),
    path(r'login/', views.login),
    path(r'register/', views.reg),
    path(r'userlist/', views.userlist),
    path(r'edit_user/', views.edit_user),
    path(r'delete_user/', views.delete_user),
    #json相关
    path(r'ab_json/', views.ab_json),
    # 上传文件
    path(r'ab_file/', views.ab_file),
    #CBV路由
    path('mylogin/', views.MyLogin.as_view())
]