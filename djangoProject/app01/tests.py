from django.test import TestCase

# Create your tests here.
import os
import sys

if __name__ == '__main__':
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'djangoProject.settings')
    import django
    django.setup()

    from app01 import  models
    print(models.User.objects.all())