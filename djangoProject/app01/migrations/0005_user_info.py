# Generated by Django 2.2.5 on 2021-10-04 11:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0004_user_age'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='info',
            field=models.CharField(max_length=32, null=True, verbose_name='个人简介'),
        ),
    ]
