# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.



#有参函数
def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

# 空函数
def func(x,y):
    pass

# 返回值
def add(x,y):
    res = x +y
    return res

# 返回多个参数
def func_m():
    return 10, "123", [1,2,3]

# 可变长度参数
# *会接受溢出的参数, 元组的方式
def func_c(x, y, *args):
    print(x,y,args)


# 接收溢出的关键字实参
# 字典的形式
def func_a(x, y, **kwargs):
    print(x,y,kwargs)


def func(x,y,z):
    print(x,y,z)

# 混用*与** *args必须在**kwargs前面
def funcc(*args, **kwargs):
    print(args, kwargs)

# 命名关键字实参必须按照key=vaule形式传递
def funcx(x,y, *args ,a, b):
    print(x,y,args, a, b)

# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

    res = add(5,10)
    print(res)

    res = add(y=15, x=10);
    print(res)

    print(func_m())

    func_c(1,2,3,4,5,6,7,7)

    func_a(1,2,a=1,b=2,c=3)


    # *先自动分散, 然后传值
    func_c(1, 2, 3, *[4,5,6])

    # *可以用在实参中, 自动分散参数
    func(*[1,2,3])

    # * for循环第一个值分散到参数中
    func(*{'x':1, 'y':2, 'z':"3"})
    # ** 会自动分配到对应的参数 x y z
    func(**{'x':1, 'y':2, 'z':"3"})

    funcc(1,2,3,4,5,6, x=1, y=2)

    funcx(1,2,a=5,b=6)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
