# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.
    while True:
        x = yield None # None作为返回值
        print(f'Hi, {x}')  # Press ⌘F8 to toggle the breakpoint.




# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    f = print_hi('Greg')
    next(f)   #第一次触发函数体运行
    next(f)   #第二次到yield
    rs = f.send('tong zhu ren')
    print("返回值",rs)
    rs = f.send(['tong', 'zhu', 'ren'])
    print("返回值 %s" % (rs))
    f.close() #关闭后无法传值



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
