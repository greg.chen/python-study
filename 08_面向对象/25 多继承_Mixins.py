# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


class Vehicle:
    pass

# # mixins机制核心：就是在多继承背景下尽可能地提升多继承的可读性
class FlyableMixin:
    def fly(self):
        print('Parent1.f1')

# 明航飞机
class CivilAircraft(FlyableMixin, Vehicle):
    pass

# 直升飞机
class Helicopter(FlyableMixin, Vehicle):
    pass

# 直升飞机
class Car(Vehicle):
    pass

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    obj1 = Car();


# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
