# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


"""
成人的BMI数值：
过轻：低于18.5
正常：18.5-23.9
过重：24-27
肥胖：28-32
非常肥胖, 高于32
　　体质指数（BMI）=体重（kg）÷身高^2（m）
　　EX：70kg÷（1.75×1.75）=22.86
"""

class Parent1:

    def f1(self):
        print('Parent1.f1')

    def f2(self):
        print('Parent1.f2')

class Parent2:
    pass

# 单继承
class Sub1(Parent1):
    def f1(self):
        print('Parent1.f1')

# 多继承
class Sub2(Parent1, Parent2):
    pass


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    print(Sub1.__bases__)
    print(Sub2.__bases__)

    obj1 = Sub1();
    obj1.f2()

# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
