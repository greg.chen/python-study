# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

# 执行系统命令
import logging



def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    logging.debug('debug')
    logging.info('info')
    logging.warning('warning')
    logging.error('error')
    logging.critical('critical')


# Press the green button in  the gutter to run the script.
if __name__ == '__main__':

    logging.basicConfig(
        # 1、日志输出位置：1、终端 2、文件
        filename='access.log',  # 不指定，默认打印到终端

        # 2、日志格式
        format='%(asctime)s - %(name)s - %(levelname)s -%(module)s:  %(message)s',

        # 3、时间格式
        datefmt='%Y-%m-%d %H:%M:%S %p',

        # 4、日志级别
        # critical => 50
        # error => 40
        # warning => 30
        # info => 20
        # debug => 10
        level=10,
    )

    print_hi('JJ')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
