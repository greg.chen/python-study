# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

# 1. 执行m.py
# 2. 运行过程中产生的名字放入名称空间
# 3.
import time
import datetime

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    # 1970年到现在
    print(time.time())
    # 时间格式化
    print(time.strftime('%Y-%m-%d %H:%M:%S'))
    print(time.strptime('2021-08-13 23:02:39','%Y-%m-%d %H:%M:%S'))

    # 结构化时间
    res = time.localtime()
    print(res.tm_year, res.tm_mon, res.tm_mday)

    print(datetime.datetime.now())

    #时间运算
    print(datetime.datetime.now() + datetime.timedelta(days=-3))
    print(datetime.datetime.now() + datetime.timedelta(weeks=1))

    #时间格式转换



# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
