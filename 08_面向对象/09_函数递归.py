# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.
    #print_hi(name)


def f1(n):
    if n == 10:
        return
    print(n)
    n +=1
    f1(n)

def age(n):
    if n == 1:
        return 18

    return age(n-1) + 10


# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')
    f1(1)

    res = age(5)
    print(res)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
