# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


"""
成人的BMI数值：
过轻：低于18.5
正常：18.5-23.9
过重：24-27
肥胖：28-32
非常肥胖, 高于32
　　体质指数（BMI）=体重（kg）÷身高^2（m）
　　EX：70kg÷（1.75×1.75）=22.86
"""

class People:

    def __init__(self,name, weight, height):
        self.__name = name
        self.weight = weight
        self.height = height
        print(name, weight,height)


    @property
    def bmi(self):
        return self.weight /(self.height ** 2)

    def get_name(self):
        return self.__name

    def set_name(self, val):
        if type(val) is not str:
            print('必须传入str类型')
        self.__name = val

    def del_name(self):
        del self.__name

    name123 = property(get_name, set_name, del_name)

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, val):
        if type(val) is not str:
            print('必须传入str类型')
        self.__name = val

    @name.deleter
    def name(self):
        del self.__name


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    obj1 = People('Greg', 70 ,1.6)
    print('bmi', obj1.bmi)

    print(obj1.get_name())
    obj1.set_name("JJ")
    print(obj1.get_name())
    print(obj1.name)


# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
