# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.
    #print_hi(name)


def binary_search(find_num, l):
    print(l)

    if len(l) == 0 :
        print('not find it')
        return

    mid_index = len(l) // 2
    mid_val = l[mid_index]

    if find_num > mid_val:
        l = l[mid_index +1 :]
        binary_search(find_num, l) #顾头不顾尾
    elif find_num < mid_val:
        l = l[:mid_index]
        binary_search(find_num, l)  # 顾头不顾尾
    else:
        print('find it', mid_index)


# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')

    nums = [-3,4,7,10,13,21,44,2,5,11]
    nums.sort()
    find_num = 130
    binary_search(find_num, nums)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
