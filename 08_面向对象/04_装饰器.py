# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

import time




def outter(func):

    def wrapper(*args, **kwargs):  # args=(1,2,3)
        start = time.time()
        time.sleep(1)
        #   kwargs={'a':1, 'b':2}
        res = func(*args, **kwargs)  # index(*(1,2,3) ,  **{'a':1, 'b':2}))
        # 打撒 index(1,2,3,  a=1, b=2)
        stop = time.time()
        print(stop-start)
        return res

    return wrapper


def deco(func):

    def wrapper(*args, **kwargs):
        res = func(*args, **kwargs)
        print("deco")
        return res

    return wrapper;


#加载顺序自下而上
@outter
@deco
def index(x, y):
    print(x, y)


#有参函数

@outter
@deco
def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.



# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('Greg')


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
