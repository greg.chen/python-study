# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

# 1. 执行m.py
# 2. 运行过程中产生的名字放入名称空间
# 3.
import os


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    # 获取文件夹下所有的子文件及子文件夹
    res = os.listdir('.')
    print(res)

    size = os.path.getsize('m.py')
    print(size)

    print(__file__)
    print(os.path.abspath(__file__))

    BASE_DIR = os.path.dirname(os.path.dirname(__file__))
    print(BASE_DIR)

# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
