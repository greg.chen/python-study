# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

# 1. 执行m.py
# 2. 运行过程中产生的名字放入名称空间
# 3.

import json
import pickle

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    json_res = json.dumps({"list": [1,2,3,4,'aa']})
    print(json_res)

    l = json.loads(json_res)
    print(l)

    with open('test.json', mode='wt', encoding='utf-8') as f:
        json.dump({"list": [1,2,3,4,'aa']},f)

    with open('test.json', mode='rt', encoding='utf-8') as f:
        l = json.load(f)
        print("load from file",l)

    res = pickle.dumps({"list": [1,2,3,4,'aa']})
    print(res)

    s = pickle.loads(res)
    print('pickle',s)

# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
