# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

# 1. 执行m.py
# 2. 运行过程中产生的名字放入名称空间
# 3.



import m
from m import f2
from m import x
x = 111
import sys

import mmm

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.
    #print_hi(name)
    # 模块名+方法名
    m.f1()
    print(m.x)


# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')
    f2()
    print(x)
    print(sys.path)
    mmm.say()
    mmm.f3()
    mmm.f4()


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
