# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.





# Press the green button in  the gutter to run the script.
if __name__ == '__main__':

    x = 3
    y = 2
    res = x if x > y else y
    print_hi(res)
    res = 1111 if x > y else 22222
    print_hi(res)


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
