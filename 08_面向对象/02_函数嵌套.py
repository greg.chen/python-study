# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.



#有参函数
def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.


# 求园的周长和面积
def circle(radius, action=0):
    from math import pi

    def perimiter(radius):
        return 2*pi*radius

    def area(radius):
        return pi*(radius**2)

    if action == 0:
        return perimiter(radius)
    elif action == 1:
        return area(radius)


# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

    print("perimiter",circle(33, action=0))
    print("area",circle(33, action=1))

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
