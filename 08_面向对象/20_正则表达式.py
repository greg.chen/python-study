# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

# 执行系统命令
import re

# 字符	描述
# .	匹配除“\n”之外的任意单个字符
# \d	匹配0到9之间的一个数字，等价于[0-9]
# \D	匹配一个非数字字符，等价于[^0-9]
# \s	匹配任意空白字符，如空格、制表符“\t”、换行“\n”等
# \S	匹配任意非空白字符
# \w	匹配任意字母或者数字或下划线，如a-z，A-Z，0-9，_等
# \W	匹配任意非单词字符，等价于[^ a-zA-Z0-9_]
# [ ]	匹配[ ]中列举的字符
# ^	取反

# 字符	描述
# ^	匹配字符串开头
# $	匹配字符串结尾
# \b	匹配一个单词的边界
# \B	匹配非单词边界

# 贪婪模式：
# 正则引擎默认是贪婪模式，尽可能多的匹配字符。
# 非贪婪模式
# 与贪婪模式相反，尽可能少的匹配字符
# 在表示数量的“*”,“?”,“+”,“{m,n}”符号后面加上?，使贪婪变成非贪婪。

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.
    # 字符
    print(re.findall('\w', 'aAbc123_*()'))
    # 特殊字符
    print(re.findall('\W', 'aAbc123_*()'))
    # 空白字符
    print(re.findall('\s', 'aAbc\t\n\r123 _*()'))
    # 非空白字符
    print(re.findall('\S', 'aAbc\t\n\r123 _*()'))
    # 数字
    print(re.findall('\d', 'aAbc123_*()'))
    # 非数字
    print(re.findall('\D', 'aAbc123_*()'))
    # 从头开始匹配字符串
    print(re.findall('\AaAbc', 'aAbc123_*()'))
    # 从尾部开始匹配字符串
    print(re.findall('bb\Z', 'aAbc123_*bb'))
    # 从头开始匹配字符串
    print(re.findall('^aAbc', 'aAbc123_*()'))
    # 从尾部开始匹配字符串
    print(re.findall('bb$', 'aAbc123_*bb'))
    # 从头aAb, 尾c匹配
    print(re.findall('^aAbc$', 'aAbc'))
    # 重复匹配
    #. : 匹配任意一个字符, 除\n
    print(re.findall('a.b', 'a1b a2b aaa bbb a\nb a\nc'))
    # *: 左侧字符重复0次或无穷次 贪婪
    print(re.findall('ab*', 'a ab abbb abbbbb'))
    # +: 左侧字符重复1次或无穷次 贪婪
    print(re.findall('ab+', 'a ab abbb abbbbb'))
    # ?: 左侧字符重复0次或1次
    print(re.findall('ab?', 'a ab abbb abbbbb'))
    # {n,m} 左侧字符重复n次或m次
    # {0,} == *
    # {1,} == +
    # {0,1} == ?
    print(re.findall('ab{3,5}', 'a ab abbb abbbbb'))
    # 查找整数和小数
    print(re.findall('\d+\.{0,1}\d+', 'adsadf1`23142123bb1.1ass1231.31..'))
    #[]匹配指定字符一个
    print(re.findall('a[5012345]b', 'a a2b a2b1bb a4bbbbb'))
    print(re.findall('a[0-5]b', 'a a2b a2b1bb a4bbbbb'))
    print(re.findall('a[0-9a-zA-Z]b', 'a a2b a2b1bb a4baZbbbb'))

# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
