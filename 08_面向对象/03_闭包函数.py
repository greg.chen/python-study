# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.



#有参函数
def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

x= 0000
#"闭"函数指的是该函数是内嵌函数
#"包"函数指该函数包含对外层函数作用域名字的引用
def f1(x):

        def f2():  #包函数, 在定义阶段引用x
            print(x)

        f2()



# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')
    x = 2222
    f1(333)


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
