# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

# 1. 执行m.py
# 2. 运行过程中产生的名字放入名称空间
# 3.
import np

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    theta = np.arange(0, 2*np.pi, 0.01)
    print(theta)


# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
