# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    l = ['aa','bbaa','ccaa','dd','zz']

    new_l = [name for name in l if name.endswith('aa')]
    print(new_l)

    new_l = [name.upper() for name in l]
    print(new_l)

# 生成0到29间所有奇数的平方放到一个列表中
def squared(x):
    return x*x



# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')
    multiples = [squared(i) for i in range(30) if i % 2 != 0]
    print(multiples)
    print(9 > 8 or 10 > 12 and not 2 + 2 > 3 )

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
