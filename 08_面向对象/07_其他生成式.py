# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    keys=['name','age', 'gender']
    #字典
    dic = {key:123 for key in keys}
    print(dic)

    items=[('name','JJ'), ('age', 18)]

    res = [ item for item in items if item[0]=='name']
    print(res)

    #字典
    res = { k:v for k,v in items}
    print("字典",res)

    #集合
    set1 = {key for key in keys}
    print(set1)

    #生成器, next取值
    tuple1 =  (key for key in keys)
    print(next(tuple1))
    print(next(tuple1))
    print(next(tuple1))

# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')

    #普通方式
    with open("05_yield.py", mode="rt", encoding="utf-8") as f:
        res = 0
        for line in f:
            res += len(line)
        print(res)

    # 效率最高, 使用生成器
    with open("05_yield.py", mode="rt", encoding="utf-8") as f:
        res = sum((len(line) for line in f))
        print(res)



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
