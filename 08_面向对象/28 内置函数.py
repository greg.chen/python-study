# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.


class Course:
    def __init__(self,name,period,price):
        self.name=name
        self.period=period
        self.price=price

    def tell_info(self):
        print('<课程名:%s 周期:%s 价钱:%s>' %(self.name,self.period,self.price))


# Press the green button in  the gutter to run the script.
if __name__ == '__main__':

    print(abs(-1))
    print(all([1,None,3,4]))
    print(any([0,None,1]))
    print(bin(11))
    print(oct(11))
    print(hex(11))

    def func():
        pass

    print(callable(func))

    print(chr(65))
    print(ord('A'))

    obj = Course('Greg','1',118);
    print(dir(obj))

    # 商 余数
    print(divmod(100,3))

    for i,v in enumerate(['a','b','c']):
        print(i, v)

    # 执行字符串中的表达式
    res = eval('1+2')
    print(res)

    # 不可变集合
    frozenset({1,2,3,4})

    print(isinstance(obj, Course))


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
