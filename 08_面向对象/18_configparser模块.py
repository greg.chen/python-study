# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

# 加载某种特殊格式
import configparser

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    config = configparser.ConfigParser()
    config.read('test.ini')
    print(config.sections())
    # 返回列表Key
    print(config.options('section1'))
    # 返回列表->元组
    print(config.items('section1'))
    # 获取指定值
    print(config.get('section1', 'user'))

    # 获取指定值
    print(config.getint('section1', 'age'))

# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
