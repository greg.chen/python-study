# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.
    #print_hi(name)



# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')

    res = (lambda x, y:x+y)(1,2)
    print(res)

    func = lambda x, y:x+y
    res = func(1,2)
    print(res)

    dic = {'jj':1000,'gg':12000, 'greg':1234234, 'duoduo':6666}
    dic = sorted(dic, key=lambda k:dic[k])
    print(dic)

    # map
    l = ['jj', 'duoduo', 'gg', 'greg']
    new_l = [ name+'123' for name in l]
    print(new_l)
    new_l = list(map(lambda item:item+'456',l)) # 生成器
    print(new_l)
    #filter

    #reduce


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
