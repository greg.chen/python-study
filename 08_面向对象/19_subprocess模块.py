# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

# 执行系统命令
import subprocess

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    obj = subprocess.Popen('ls ./', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    print("stderr:",obj.stderr.read())
    print("stdout:",obj.stdout.read().decode('utf-8'))



# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
