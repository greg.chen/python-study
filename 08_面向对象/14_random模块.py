# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

# 1. 执行m.py
# 2. 运行过程中产生的名字放入名称空间
# 3.
import random

#
# random()
# 0.0到1.0随机数
#
# uniform(n, m)
# n到m随机浮点数
#
# randint(n, m)
# n到m随机整数
#
# randrange(n, m, x)
# n到m之间步长为x的随机数
#
# choice()
# 随机选择一个元素
#
# shuffle()
# 混洗
#
# sample()
# 取样若干元素

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

    print(random.random())
    print(random.randint(1,10))
    # 1 或 23 或 [4,5]
    print("choice:",random.choice([1,'23', [4,5]]))
    #列表任意两个组合
    print("sample",random.sample([1,'23', [4,5]], 2))
    # 大于, 小于3 小数
    print(random.uniform(1,3))

    # 随机验证码
    res = ''
    for i in range(6):
        s1 = chr(random.randint(65,90))
        s2 = str(random.randint(0,9))
        res += random.choice([s1,s2])

    print(res)


# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('JJ')
    print(bin(id('jj')))

    while True:
        a = 1+1
        print(a)
        b = input(">>>")

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
