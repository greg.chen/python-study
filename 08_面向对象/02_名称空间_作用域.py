# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.



#有参函数
def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.



xxx = 111
def func():
    global xxx  # 全局类型
    xxx = 222
    print(xxx)

x = 1

input = 111
def f1():
    input = 222
    def f2():
        input = 333
        print(input)

    f2()


# 1. 内置名称空间
    # built-in function input
    # python解释器启动时产生. 例子: print函数

# 2. 全局名称空间
    # 不是函数类定义
    # 不是内置的
    # 运行顶级代码产生的名字
        # x = 10  (顶级)
        # if 13 > 3:
        #     y=20

        # def func():  (函数的内存地址)
        #     a = 111
        #     b = 222
        #
        # class Foo:
        #     pass

# 3. 局部名称空间
    # 调用函数内的名字
    # 调用函数后销毁

# Press the green button in  the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')
    func()
    f1()
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
